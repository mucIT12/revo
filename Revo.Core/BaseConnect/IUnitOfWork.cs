﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Revo.Core.BaseConnect
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
        Task<int> CommitAsync();
    }
}
