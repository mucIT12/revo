﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.Core.ViewModels
{
    public class TimeRange
    {
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
