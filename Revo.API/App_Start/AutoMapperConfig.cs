﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Revo.DataService.AutoMapperModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Revo.API.App_Start
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.ConfigAccountModule();
                mc.ConfigBrandModule();
                mc.ConfigBusinessModule();
                mc.ConfigReviewerModule();
                mc.ConfigReviewerModule();
                mc.ConfigCampaignModule();
                mc.ConfigIndustryModule();
                mc.ConfigIndustryTypeModule();
                mc.ConfigPlatformModule();
                mc.ConfigVoucherModule();
                mc.ConfigVoucherMappingReviewerModule();
                mc.ConfigCampaignApplyModule();
                mc.ConfigPostModule();
                mc.ConfigReviewerMappingBrandModule();
                mc.ConfigPlatformMappingReviewerModule();
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
