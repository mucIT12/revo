﻿using Microsoft.Extensions.DependencyInjection;
using Revo.DataService.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Revo.API.App_Start
{
    public static class DependencyInjectionResolver
    {
        public static void ConfigureDI(this IServiceCollection services)
        {
            services.InitializerDI();
            //services.ConfigServicesDI();
        }
    }
}
