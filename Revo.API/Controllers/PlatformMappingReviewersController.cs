using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/platform-mapping-reviewers")]
    public partial class PlatformMappingReviewersController : ControllerBase
    {
        private readonly IPlatformMappingReviewerService _platformMappingReviewerService;
        private readonly IConfigurationProvider _mapper;
        public PlatformMappingReviewersController(IPlatformMappingReviewerService platformMappingReviewerService, IMapper mapper)
        {
            _platformMappingReviewerService = platformMappingReviewerService;
            _mapper = mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter platformMappingReviewers
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>PlatformMappingReviewerViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformMappingReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] PlatformMappingReviewerViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _platformMappingReviewerService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get platform by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>PlatformMappingReviewerViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformMappingReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _platformMappingReviewerService.GetById(id));
        }
        /// <summary>
        /// create platformMappingReviewer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>PlatformMappingReviewerModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformMappingReviewerModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(PlatformMappingReviewerModel entity)
        {
            var result = await _platformMappingReviewerService.CreatePlatformMappingReviewer(entity);
            return Ok(result);
        }
        /// <summary>
        /// update platformMappingReviewer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformMappingReviewerUpdateModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, PlatformMappingReviewerUpdateModel entity)
        {
            await _platformMappingReviewerService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete platformMappingReviewer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _platformMappingReviewerService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count platformMappingReviewer
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_platformMappingReviewerService.Count());
        }
        /// <summary>
        /// check connect Platform by reviewerId and platformId
        /// </summary>
        /// <param name="reviewerId"></param>
        /// <param name="platformId"></param>
        /// <returns></returns>
        [HttpGet("{reviewerId}/{platformId}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckFavouritePlatform(int reviewerId, int platformId)
        {
            var check = await _platformMappingReviewerService.CheckFavouritePlatform(reviewerId, platformId);
            if (check == 0) return Ok("True");
            if (check == 1) return Ok("False");
            return null;
        }
    }
}