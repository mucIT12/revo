using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/reviewer-mapping-brands")]
    public partial class ReviewerMappingBrandsController : ControllerBase
    {
        private readonly IReviewerMappingBrandService _reviewerMappingBrandService;
        private readonly IConfigurationProvider _mapper;
        public ReviewerMappingBrandsController(IReviewerMappingBrandService reviewerMappingBrandService,IMapper mapper){
            _reviewerMappingBrandService=reviewerMappingBrandService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter reviewerMappingBrands
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>ReviewerMappingBrandViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerMappingBrandViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] ReviewerMappingBrandViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _reviewerMappingBrandService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get reviewerMappingBrand by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ReviewerMappingBrandViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerMappingBrandViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _reviewerMappingBrandService.GetById(id));
        }
        /// <summary>
        /// create reviewerMappingBrand
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>ReviewerMappingBrandModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerMappingBrandModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(ReviewerMappingBrandModel entity)
        {
            var result = await _reviewerMappingBrandService.CreateReviewerMappingBrand(entity);
            return Ok(result);
        }
        /// <summary>
        /// update reviewerMappingBrand
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        public IActionResult Update(int id,ReviewerMappingBrand entity)
        {
            _reviewerMappingBrandService.Update(entity);
            return Ok();
        }
        /// <summary>
        /// delete reviewerMappingBrand
        /// </summary>
        /// <param name="reviewerId"></param>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpDelete("{reviewerId}/{brandId}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int reviewerId, int brandId)
        {
            await _reviewerMappingBrandService.Delete(reviewerId, brandId);
            return Ok("Deleted");
        }
        /// <summary>
        /// count reviewerMappingBrand
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_reviewerMappingBrandService.Count());
        }
        /// <summary>
        /// check favourite brand
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet("check-favourite")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerMappingBrandCheckModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckFavouriteBrand([FromQuery] ReviewerMappingBrandCheckModel model)
        {
            var result = await _reviewerMappingBrandService.CheckFavouriteBrand(model);
            return Ok(result);
        }
    }
}
