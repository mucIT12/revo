using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/vouchers")]
    public partial class VouchersController : ControllerBase
    {
        private readonly IVoucherService _voucherService;
        private readonly IConfigurationProvider _mapper;
        public VouchersController(IVoucherService voucherService,IMapper mapper){
            _voucherService=voucherService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter vouchers
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>VoucherViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] VoucherViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _voucherService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get voucher by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>VoucherViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _voucherService.GetById(id));
        }
        /// <summary>
        /// create voucher
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>VoucherModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(VoucherModel entity)
        {
            var result = await _voucherService.CreateVoucher(entity);
            return Ok(result);
        }
        /// <summary>
        /// update voucher
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, VoucherViewModel entity)
        {
            await _voucherService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete voucher
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _voucherService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count voucher
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_voucherService.Count());
        }
    }
}
