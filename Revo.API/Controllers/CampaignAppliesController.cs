using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/campaign-applies")]
    public partial class CampaignAppliesController : ControllerBase
    {
        private readonly ICampaignApplyService _campaignApplyService;
        private readonly IConfigurationProvider _mapper;
        public CampaignAppliesController(ICampaignApplyService campaignApplyService,IMapper mapper){
            _campaignApplyService=campaignApplyService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter camapaignApply
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>CampaignApplyViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignApplyViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] CampaignApplyViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _campaignApplyService.GetAll(model, fields, page, size));
        }
        ///// <summary>
        ///// filter all post of reviewer
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="fields"></param>
        ///// <param name="page"></param>
        ///// <param name="size"></param>
        ///// <returns>CampaignApplyPostViewModel</returns>
        //[HttpGet("get-all-post")]
        //[MapToApiVersion("1")]
        //[Authorize]
        //[ProducesResponseType(typeof(DynamicModelsResponse<CampaignApplyPostViewModel>), (int)HttpStatusCode.OK)]
        //public async Task<IActionResult> GetAllPost([FromQuery] CampaignApplyPostViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        //{
        //    return Ok(await _campaignApplyService.GetAllPost(model, fields, page, size));
        //}
        /// <summary>
        /// get compaignApply by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignApplyViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _campaignApplyService.GetById(id));
        }
        /// <summary>
        /// create campaignApply
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>CampaignApplyModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignApplyModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(CampaignApplyModel entity)
        {
            var result = await _campaignApplyService.CreateCampaignApply(entity);
            return Ok(result);
        }
        /// <summary>
        /// update accept status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}/accept-status")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, CampaignApplyUpdateAcceptStatusModel entity)
        {
            await _campaignApplyService.UpdateAcceptStatus(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// update complete status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}/complete-status")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, CampaignApplyUpdateCompleteStatusModel entity)
        {
            await _campaignApplyService.UpdateCompleteStatus(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// delete campaignApply
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            await _campaignApplyService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// delete campaignApply by reviewerId and campaignId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{reviewerId}/{campaignId}")]
        [MapToApiVersion("1")]
        [Authorize]
        public async Task<IActionResult> DeleteById(int reviewerId, int campaignId)
        {
            await _campaignApplyService.DeleteById(reviewerId, campaignId);
            return Ok("Deleted");
        }
        /// <summary>
        /// count campaignApply
        /// </summary>
        /// <returns></returns>
        [HttpGet("count/{campaignId}")]
        [MapToApiVersion("1")]
        [Authorize]
        public async Task<IActionResult> Count(int campaignId)
        {
            var result = await _campaignApplyService.Count(campaignId);
            return Ok(result);
        }
        /// <summary>
        /// count campaignApply by status
        /// </summary>
        /// <returns></returns>
        [HttpGet("count-status/{reviewerId}/{status}")]
        [MapToApiVersion("1")]
        [Authorize]
        public async Task<IActionResult> Count(int reviewerId, int status)
        {
            var result = await _campaignApplyService.CountStatus(reviewerId, status);
            return Ok(result);
        }
        /// <summary>
        /// check status of campaign Apply
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="reviewerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/{reviewerId}")]
        [MapToApiVersion("1")]
        [Authorize]
        public async Task<IActionResult> CheckCampaign(int campaignId, int reviewerId)
        {
            var check = await _campaignApplyService.CheckStatusCampaignApply(campaignId, reviewerId);
            if (check == 0) return Ok("True");
            if (check == 1) return Ok("False");
            return null;
        }
        /// <summary>
        /// get campaign apply by camapginId and reviewerId
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="reviewerId"></param>
        /// <returns></returns>
        [HttpGet("get-id/{campaignId}/{reviewerId}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignApplyViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetIdCampaignApply(int campaignId, int reviewerId)
        {
            return Ok(await _campaignApplyService.GetIdCampaignApply(campaignId, reviewerId));
        }
    }
}
