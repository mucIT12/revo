using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/platforms")]
    public partial class PlatformsController : ControllerBase
    {
        private readonly IPlatformService _platformService;
        private readonly IConfigurationProvider _mapper;
        public PlatformsController(IPlatformService platformService,IMapper mapper){
            _platformService=platformService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter platforms
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>PlatformViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] PlatformViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _platformService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get platform by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>PlatformViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _platformService.GetById(id));
        }
        /// <summary>
        /// create platform
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>PlatformModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(PlatformModel entity)
        {
            var result = await _platformService.CreatePlatform(entity);
            return Ok(result);
        }
        /// <summary>
        /// update platform
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<PlatformViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, PlatformViewModel entity)
        {
            await _platformService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete platform
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _platformService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count platform
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_platformService.Count());
        }
    }
}
