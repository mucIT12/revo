using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.Core.Custom;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/accounts")]
    public partial class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IConfigurationProvider _mapper;
        public AccountsController(IAccountService accountService,IMapper mapper){
            _accountService=accountService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns>TokenModel</returns>
        [HttpPost("login")]
        [MapToApiVersion("1")]
        public IActionResult Login([FromBody] LoginModel model)
        {
            var result = _accountService.LoginAsync(model);
            if(result == null)
            {
                return NotFound("Invalid username or password");
            }
            return Ok(result);
        }
        /// <summary>
        /// filter account
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>AccountViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<AccountViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] AccountViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _accountService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get account by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>AccountViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<AccountViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _accountService.GetById(id));
        }
        /// <summary>
        /// create account reviewer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>AccountModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [ProducesResponseType(typeof(DynamicModelsResponse<AccountModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(AccountModel entity)
        {
            var result = await _accountService.CreateAccount(entity);
            return Ok(result);
        }
        /// <summary>
        /// create account reviewer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>AccountCreateBusinessModel</returns>
        [HttpPost("create-business")]
        [MapToApiVersion("1")]
        [ProducesResponseType(typeof(DynamicModelsResponse<AccountCreateBusinessModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(AccountCreateBusinessModel entity)
        {
            var result = await _accountService.CreateAccountBusiness(entity);
            return Ok(result);
        }
        /// <summary>
        /// update account
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<AccountUpdateModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, AccountUpdateModel entity)
        {
            await _accountService.Update(id,entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _accountService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count account
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_accountService.Count());
        }
        /// <summary>
        /// check current password
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentPassword"></param>
        /// <returns></returns>
        [HttpGet("{id}/{currentPassword}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckFavouritePlatform(int id, string currentPassword)
        {
            var check = await _accountService.CheckCurrentPassword(id, currentPassword);
            if (check == 0) return Ok("True");
            if (check == 1) return Ok("False");
            return null;
        }
        /// <summary>
        /// un-ban
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}/un-ban")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateUnban(int id, AccountUnbanModel entity)
        {
            await _accountService.UpdateUnban(id, entity);
            return Ok("Update Success!");
        }
    }
}
