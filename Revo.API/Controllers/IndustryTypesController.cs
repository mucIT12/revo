using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/industry-types")]
    public partial class IndustryTypesController : ControllerBase
    {
        private readonly IIndustryTypeService _industryTypeService;
        private readonly IConfigurationProvider _mapper;
        public IndustryTypesController(IIndustryTypeService industryTypeService,IMapper mapper){
            _industryTypeService=industryTypeService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter industryType
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] IndustryTypeViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _industryTypeService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get industryType by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IndustryTypeViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _industryTypeService.GetById(id));
        }
        /// <summary>
        /// create industryType
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>IndustryTypeModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryTypeModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(IndustryTypeModel entity)
        {
            var result = await _industryTypeService.CreateIndustryType(entity);
            return Ok(result);
        }
        /// <summary>
        /// update industryType
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryTypeViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, IndustryTypeViewModel entity)
        {
            await _industryTypeService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete industryType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _industryTypeService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count industryType
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_industryTypeService.Count());
        }
    }
}
