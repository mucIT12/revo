using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/reviewers")]
    public partial class ReviewersController : ControllerBase
    {
        private readonly IReviewerService _reviewerService;
        private readonly IConfigurationProvider _mapper;
        public ReviewersController(IReviewerService reviewerService,IMapper mapper){
            _reviewerService=reviewerService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter reviewers
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ReviewerViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] ReviewerViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _reviewerService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get reviewer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ReviewerViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _reviewerService.GetById(id));
        }
        /// <summary>
        /// get reviewer by accountId
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>ReviewerViewModel</returns>
        [HttpGet("get-by-accountId/{accountId}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetByAccountId(int accountId)
        {
            return Ok(await _reviewerService.GetByAccountId(accountId));
        }
        /// <summary>
        /// create reviewer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>ReviewerModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(ReviewerModel entity)
        {
            var result = await _reviewerService.CreateReviewer(entity);
            return Ok(result);
        }
        /// <summary>
        /// update reviewer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerUpdateModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, ReviewerUpdateModel entity)
        {
            await _reviewerService.Update(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// update image reviewer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}/update-image")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<ReviewerUpdateImageModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateImage(int id, ReviewerUpdateImageModel entity)
        {
            await _reviewerService.UpdateImage(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// delete reviewer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _reviewerService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count reviewer
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_reviewerService.Count());
        }
    }
}
