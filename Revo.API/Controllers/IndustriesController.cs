using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/industries")]
    public partial class IndustriesController : ControllerBase
    {
        private readonly IIndustryService _industryService;
        private readonly IConfigurationProvider _mapper;
        public IndustriesController(IIndustryService industryService,IMapper mapper){
            _industryService=industryService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter industry
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>IndustryViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] IndustryViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _industryService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get industry by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IndustryViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _industryService.GetById(id));
        }
        /// <summary>
        /// create industry
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(IndustryModel entity)
        {
            await _industryService.CreateIndustry(entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// update industry
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns>IndustryViewModel</returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<IndustryViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, IndustryViewModel entity)
        {
            var result = await _industryService.Update(id, entity);
            return Ok(result);
        }
        /// <summary>
        /// delete industry
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _industryService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count industry
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_industryService.Count());
        }
    }
}
