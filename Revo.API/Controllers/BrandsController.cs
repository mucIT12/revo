using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/brands")]
    public partial class BrandsController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IConfigurationProvider _mapper;
        public BrandsController(IBrandService brandService,IMapper mapper){
            _brandService=brandService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter brands
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>BrandViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BrandViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] BrandViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _brandService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get Brand by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>BrandViewModel</returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BrandViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _brandService.GetById(id));
        }
        /// <summary>
        /// create brand
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>BrandModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BrandModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(BrandModel entity)
        {
            var result = await _brandService.CreateBrand(entity);
            return Ok(result);
        }
        /// <summary>
        /// update brand
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BrandUpdateModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, BrandUpdateModel entity)
        {
            await _brandService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// delete brand
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _brandService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count brands
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_brandService.Count());
        }

        /// <summary>
        /// filter name brands
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>BrandNameModel</returns>
        [HttpGet("get-name")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsDataResponse<BrandNameModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetsName([FromQuery] BrandNameModel model)
        {
            return Ok(await _brandService.GetAllName(model));
        }
    }
}
