using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/businesss")]
    public partial class BusinesssController : ControllerBase
    {
        private readonly IBusinessService _businessService;
        private readonly IConfigurationProvider _mapper;
        public BusinesssController(IBusinessService businessService,IMapper mapper){
            _businessService=businessService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter Business
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>BusinessViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BusinessViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] BusinessViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _businessService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get business by accountId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>BusinessViewModel</returns>
        [HttpGet("{accountId}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BusinessViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int accountId)
        {
            return Ok(await _businessService.GetById(accountId));
        }
        /// <summary>
        /// create business
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>BusinessModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BusinessModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(BusinessModel entity)
        {
            var result = await _businessService.CreateBusiness(entity);
            return Ok(result);
        }
        /// <summary>
        /// update business
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<BusinessViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, BusinessViewModel entity)
        {
            await _businessService.Update(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// delete business
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _businessService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count business
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_businessService.Count());
        }
    }
}
