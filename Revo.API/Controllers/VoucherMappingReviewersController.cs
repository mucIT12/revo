﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.API.Controllers
{
    [Route("api/v{version:apiVersion}/voucher-mapping-reviewers")]
    [ApiVersion("1")]
    [ApiController]
    public class VoucherMappingReviewersController : ControllerBase
    {
        private readonly IVoucherMappingReviewerService _voucherMappingReviewerService;
        private readonly IConfigurationProvider _mapper;
        public VoucherMappingReviewersController(IVoucherMappingReviewerService voucherMappingReviewerService, IMapper mapper)
        {
            _voucherMappingReviewerService=voucherMappingReviewerService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter voucher-mapping-reviewer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>VoucherViewModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherMappingReviewerViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] VoucherMappingReviewerViewModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _voucherMappingReviewerService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// create voucher-mapping-reviewer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<VoucherMappingReviewerModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(VoucherMappingReviewerModel entity)
        {
            var result = await _voucherMappingReviewerService.CreateVoucherMappingReviewer(entity);
            return Ok(result);
        }
    }
}
