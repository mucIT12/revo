using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Response;
using Revo.DataService.Services;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Revo.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/campaigns")]
    public partial class CampaignsController : ControllerBase
    {
        private readonly ICampaignService _campaignService;
        private readonly IConfigurationProvider _mapper;
        public CampaignsController(ICampaignService campaignService,IMapper mapper){
            _campaignService=campaignService;
            _mapper= mapper.ConfigurationProvider;
        }
        /// <summary>
        /// filter campaign
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns>CampaignViewAllModel</returns>
        [HttpGet]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignViewAllModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Gets([FromQuery] CampaignViewAllModel model, [FromQuery] string[] fields = null, int page = CommonConstants.DefaultPage, int size = CommonConstants.DefaultPaging)
        {
            return Ok(await _campaignService.GetAll(model, fields, page, size));
        }
        /// <summary>
        /// get campaign by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignDetailViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _campaignService.GetById(id));
        }
        /// <summary>
        /// create campaign
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>CampaignModel</returns>
        [HttpPost]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create(CampaignModel entity)
        {
            var result = await _campaignService.CreateCampaign(entity);
            return Ok(result);
        }
        /// <summary>
        /// update campaign
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignUpdateModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(int id, CampaignUpdateModel entity)
        {
            await _campaignService.Update(id, entity);
            return Ok("Update Success!");
        }
        /// <summary>
        /// update status of campaign
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}/update-status")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateStatus(int id, CampaignUpdateStatusModel entity)
        {
            await _campaignService.UpdateCampaignStatus(id, entity);
            return Ok("Update Success");
        }
        /// <summary>
        /// delete campaign
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete(int id)
        {
            await _campaignService.Delete(id);
            return Ok("Deleted");
        }
        /// <summary>
        /// count campaign
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult Count()
        {
            return Ok(_campaignService.Count());
        }

        /// <summary>
        /// get all only name campaign 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet("get-name")]
        [MapToApiVersion("1")]
        [Authorize]
        [ProducesResponseType(typeof(DynamicModelsResponse<CampaignNameModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetsName([FromQuery] CampaignNameModel model)
        {
            return Ok(await _campaignService.GetAllName(model));
        }
    }
}
