﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.Enums
{
    enum CampaignEnum
    {
        InProgress = 0,
        Closed = 1,
    }
    enum RecruitmentEnum
    {
        Recruiting = 0,
        Closed = 1,
    }
    enum FeaturedEnum
    {
        NewCampaign = 0,
        FeaturedCampaign = 1,
    }
}
