﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.Enums
{
    enum PostEnum
    {
        Pending = 0,
        Confirm = 1,
        Cancel = 2
    } 
}
