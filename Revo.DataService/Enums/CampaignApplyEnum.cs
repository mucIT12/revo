﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.Enums
{
    enum CampaignApplyEnum
    {
        Applying = 0,
        InChange = 1,
        Complete = 2,
    }
}
