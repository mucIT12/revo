﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IPlatformService
    {
        Task<PlatformModel> CreatePlatform(PlatformModel model);
        Task<Platform> Update(int id, PlatformViewModel model);
        Task<Platform> Delete(int id);
        Task<DynamicModelsResponse<PlatformViewModel>> GetAll(PlatformViewModel filter, string[] fields, int page, int size);
        Task<PlatformViewModel> GetById(int id);
    }
    public partial class PlatformService
    {
        private readonly IConfigurationProvider _mapper;
        public PlatformService(IPlatformRepository repository, IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }

        public async Task<PlatformModel> CreatePlatform(PlatformModel model)
        {
            if (Get(x => x.Name == model.Name).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Platform Name is exsited");
            var entity = _mapper.CreateMapper().Map<Platform>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<Platform> Update(int id, PlatformViewModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<Platform> Delete(int id)
        {
            var entity = Get(id);
            if (entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<PlatformViewModel>> GetAll(PlatformViewModel model, string[] fields, int page, int size)
        {
            var result = Get(x => x.Active == true).ProjectTo<PlatformViewModel>(_mapper)
                .DynamicFilter(model)
                .Select<PlatformViewModel>(PlatformViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<PlatformViewModel>())
                .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<PlatformViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        public async Task<PlatformViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id && x.Active == true).ProjectTo<PlatformViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }
    }
}
