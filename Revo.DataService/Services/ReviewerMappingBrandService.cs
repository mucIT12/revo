﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IReviewerMappingBrandService
    {
        Task<ReviewerMappingBrandModel> CreateReviewerMappingBrand(ReviewerMappingBrandModel model);
        Task<ReviewerMappingBrand> Delete(int reviewerId, int brandId);
        Task<DynamicModelsResponse<ReviewerMappingBrandViewModel>> GetAll(ReviewerMappingBrandViewModel filter, string[] fields, int page, int size);
        Task<ReviewerMappingBrandViewModel> GetById(int id);
        Task<DynamicModelsDataResponse<ReviewerMappingBrandCheckModel>> CheckFavouriteBrand(ReviewerMappingBrandCheckModel model);
    }
    public partial class ReviewerMappingBrandService
    {
        private readonly IConfigurationProvider _mapper;
        public ReviewerMappingBrandService(IReviewerMappingBrandRepository repository,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }

        public async Task<ReviewerMappingBrandModel> CreateReviewerMappingBrand(ReviewerMappingBrandModel model)
        {
            var entity = _mapper.CreateMapper().Map<ReviewerMappingBrand>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<ReviewerMappingBrand> Delete(int reviewerId, int brandId)
        {
            var entity = Get(x => x.ReviewerId == reviewerId && x.BrandId == brandId).FirstOrDefault();
            if (entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            await DeleteAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<ReviewerMappingBrandViewModel>> GetAll(ReviewerMappingBrandViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<ReviewerMappingBrandViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<ReviewerMappingBrandViewModel>(ReviewerMappingBrandViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<ReviewerMappingBrandViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<ReviewerMappingBrandViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        public async Task<ReviewerMappingBrandViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id).ProjectTo<ReviewerMappingBrandViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        //public async Task<DynamicModelsDataResponse<BrandNameModel>> CheckFavouriteBrand(int reviewerId)
        //{
        //    int flag = -1;
        //    var check = await Get(x => x.ReviewerId == reviewerId).ProjectTo<ReviewerMappingBrandViewModel>(_mapper).FirstOrDefaultAsync();
        //    if (check != null)
        //        flag = 0;
        //    else
        //        flag = 1;
        //    return flag;
        //}
        public async Task<DynamicModelsDataResponse<ReviewerMappingBrandCheckModel>> CheckFavouriteBrand(ReviewerMappingBrandCheckModel model)
        {
            var result = Get(x => x.ReviewerId == model.ReviewerId).ProjectTo<ReviewerMappingBrandCheckModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<ReviewerMappingBrandCheckModel>(ReviewerMappingBrandCheckModel.Fields.ToArray().ToDynamicSelector<ReviewerMappingBrandCheckModel>());
            var rs = new DynamicModelsDataResponse<ReviewerMappingBrandCheckModel>
            {
                Data = await result.ToListAsync()
            };
            return rs;
        }
    }
}
