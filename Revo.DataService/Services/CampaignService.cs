﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface ICampaignService
    {
        Task<CampaignModel> CreateCampaign(CampaignModel model);
        Task<Campaign> Update(int id, CampaignUpdateModel model);
        Task<Campaign> UpdateCampaignStatus(int id, CampaignUpdateStatusModel model);
        Task<Campaign> Delete(int id);
        Task<DynamicModelsResponse<CampaignViewAllModel>> GetAll(CampaignViewAllModel filter, string[] fields, int page, int size);
        Task<CampaignDetailViewModel> GetById(int id);
        Task<DynamicModelsDataResponse<CampaignNameModel>> GetAllName(CampaignNameModel model);
        Task ScanCampaign();
    }
    public partial class CampaignService
    {
        private readonly IConfigurationProvider _mapper;

        public CampaignService(ICampaignRepository repository,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }

        public async Task<CampaignModel> CreateCampaign(CampaignModel model)
        {
            string message = "Dear reviewer," + "<br>" + "Hurry up to apply for the campaign: " + model.Name + "<br>" + "Description: " + model.CampaignDecription + "<br>" + "End recruitment : " + model.RecruitmentDate;
            var entity = _mapper.CreateMapper().Map<Campaign>(model);
            await CreateAsyn(entity);
            await MailUtils.SendMailGoogleSmtp("revosup1@gmail.com", "zigpc2000@gmail.com", "REVO just opened a new campaign!", message, "revosup1@gmail.com", "Zz123aa123");
            return model;
        }

        public async Task<Campaign> Update(int id, CampaignUpdateModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<Campaign> UpdateCampaignStatus(int id, CampaignUpdateStatusModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<Campaign> Delete(int id)
        {
            var entity = Get(id);
            if (entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<CampaignViewAllModel>> GetAll(CampaignViewAllModel model, string[] fields, int page, int size)
        {
            var result = Get(x => x.Active == true).ProjectTo<CampaignViewAllModel>(_mapper)
                .DynamicFilter(model)
                .Select<CampaignViewAllModel>(CampaignViewAllModel.Fields.Union(fields).ToArray().ToDynamicSelector<CampaignViewAllModel>())
                .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<CampaignViewAllModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        public async Task<CampaignDetailViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id && x.Active == true).ProjectTo<CampaignDetailViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }
        public async Task<DynamicModelsDataResponse<CampaignNameModel>> GetAllName(CampaignNameModel model)
        {
            var result = Get(x => x.Active == true).ProjectTo<CampaignNameModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<CampaignNameModel>(CampaignNameModel.Fields.ToArray().ToDynamicSelector<CampaignNameModel>());
            var rs = new DynamicModelsDataResponse<CampaignNameModel>
            {
                Data = await result.ToListAsync()
            };
            return rs;
        }

        private async Task SendNotification(string title, string body, string campaignId)
        {
            var message = new FirebaseAdmin.Messaging.Message()
            {
                Notification = new FirebaseAdmin.Messaging.Notification()
                {
                    Title = title,
                    Body = body
                },
                Data = new Dictionary<string, string>()
                {
                    {"campaignId", campaignId },
                },
                Condition = "'all' in topics"
            };
            await FirebaseAdmin.Messaging.FirebaseMessaging.DefaultInstance.SendAsync(message);
        }
        public async Task ScanCampaign()
        {
            var current = DateTime.Now;
            var next = current.AddDays(1);
            var incoming = await Get(x => x.EndDate < current.AddDays(1) && x.EndDate > current && x.CampaignStatus == (int)CampaignEnum.InProgress)
                .Select(x => x.Id).ToListAsync();
            var campaign = Get(x => x.EndDate < current.AddDays(1) && x.EndDate > current && x.CampaignStatus == (int)CampaignEnum.InProgress).FirstOrDefault();
            if (incoming != null && campaign != null)
            {
                string campaignId = incoming.FirstOrDefault().ToString();
                string title = "News form Revo";
                string body = "The campaign is about to expire in 1 days," + "\r\n" + campaign.Name;
                await SendNotification(title, body, campaignId);
            }
        }
    }
}
