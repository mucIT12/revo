﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IBusinessService
    {
        Task<BusinessModel> CreateBusiness(BusinessModel model);
        Task<Business> Update(int id, BusinessViewModel model);
        Task<Business> Delete(int id);
        Task<DynamicModelsResponse<BusinessViewModel>> GetAll(BusinessViewModel filter, string[] fields, int page, int size);
        Task<BusinessViewModel> GetById(int accountId);
    }
    public partial class BusinessService
    {
        private readonly IConfigurationProvider _mapper;
        public BusinessService(IBusinessRepository repository, IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }
        public async Task<BusinessModel> CreateBusiness(BusinessModel model)
        {
            if (Get(x => x.Name == model.Name).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Business is registerd");
            var entity = _mapper.CreateMapper().Map<Business>(model);
            await CreateAsyn(entity);
            return model;
        }
        public async Task<Business> Update(int id, BusinessViewModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }
        public async Task<Business> Delete(int id)
        {
            var entity = Get(id);
            //if (entity == null || entity.Active == false)
            //    throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            //entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<BusinessViewModel>> GetAll(BusinessViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<BusinessViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<BusinessViewModel>(BusinessViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<BusinessViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<BusinessViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }
        public async Task<BusinessViewModel> GetById(int accountId)
        {
            var result = await Get(x => x.AccountId == accountId).ProjectTo<BusinessViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }
    }
}
