﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IPlatformMappingReviewerService
    {
        Task<PlatformMappingReviewerModel> CreatePlatformMappingReviewer(PlatformMappingReviewerModel model);
        Task<PlatformMappingReviewer> Update(int id, PlatformMappingReviewerUpdateModel model);
        Task<PlatformMappingReviewer> Delete(int id);
        Task<DynamicModelsResponse<PlatformMappingReviewerViewModel>> GetAll(PlatformMappingReviewerViewModel filter, string[] fields, int page, int size);
        Task<PlatformMappingReviewerViewModel> GetById(int id);
        Task<int> CheckFavouritePlatform(int reviewerId, int platformId);
    }
    public partial class PlatformMappingReviewerService
    {
        private readonly IConfigurationProvider _mapper;
        public PlatformMappingReviewerService(IPlatformMappingReviewerRepository repository,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }

        public async Task<PlatformMappingReviewerModel> CreatePlatformMappingReviewer(PlatformMappingReviewerModel model)
        {
            if (Get(x => x.PlatformId == model.PlatformId && x.ReviewerId == model.ReviewerId).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Reviewer participated in the Platform");
            var entity = _mapper.CreateMapper().Map<PlatformMappingReviewer>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<PlatformMappingReviewer> Update(int id, PlatformMappingReviewerUpdateModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<PlatformMappingReviewer> Delete(int id)
        {
            var entity = Get(id);
            if (entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            await DeleteAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<PlatformMappingReviewerViewModel>> GetAll(PlatformMappingReviewerViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<PlatformMappingReviewerViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<PlatformMappingReviewerViewModel>(PlatformMappingReviewerViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<PlatformMappingReviewerViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<PlatformMappingReviewerViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        public async Task<PlatformMappingReviewerViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id).ProjectTo<PlatformMappingReviewerViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<int> CheckFavouritePlatform(int reviewerId, int platformId)
        {
            int flag = -1;
            var check = await Get(x => x.ReviewerId == reviewerId && x.PlatformId == platformId).ProjectTo<PlatformMappingReviewerViewModel>(_mapper).FirstOrDefaultAsync();
            if (check != null)
                flag = 0;
            else
                flag = 1;
            return flag;
        }
    }
}
