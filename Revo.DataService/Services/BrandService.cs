﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IBrandService
    {
        Task<BrandModel> CreateBrand(BrandModel model);
        Task<Brand> Update(int id, BrandUpdateModel model);
        Task<Brand> Delete(int id);
        Task<DynamicModelsResponse<BrandViewModel>> GetAll(BrandViewModel filter, string[] fields, int page, int size);
        Task<BrandViewModel> GetById(int id);
        Task<DynamicModelsDataResponse<BrandNameModel>> GetAllName(BrandNameModel model);
    }
    public partial class BrandService
    {
        private readonly IConfigurationProvider _mapper;
        public BrandService(IBrandRepository repository, IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }
        public async Task<BrandModel> CreateBrand(BrandModel model)
        {
            if (Get(x => x.Name == model.Name).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Brand name is registerd");
            var entity = _mapper.CreateMapper().Map<Brand>(model);
            await CreateAsyn(entity);
            return model;
        }
        public async Task<Brand> Update(int id, BrandUpdateModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }
        public async Task<Brand> Delete(int id)
        {
            var entity = Get(id);
            if (entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<BrandViewModel>> GetAll(BrandViewModel model, string[] fields, int page, int size)
        {
            var result = Get(x => x.Active == true).ProjectTo<BrandViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<BrandViewModel>(BrandViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<BrandViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<BrandViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }
        public async Task<BrandViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id && x.Active == true).ProjectTo<BrandViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<DynamicModelsDataResponse<BrandNameModel>> GetAllName(BrandNameModel model)
        {
            var result = Get(x => x.Active == true).ProjectTo<BrandNameModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<BrandNameModel>(BrandNameModel.Fields.ToArray().ToDynamicSelector<BrandNameModel>());
            var rs = new DynamicModelsDataResponse<BrandNameModel>
            {
                Data = await result.ToListAsync()
            };
            return rs;
        }
    }
}