﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Revo.Core.BaseConnect;
using Revo.Core.Constants;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IAccountService
    {
        TokenModel LoginAsync(LoginModel signInModel);
        Task<AccountModel> CreateAccount(AccountModel model);
        Task<AccountCreateBusinessModel> CreateAccountBusiness(AccountCreateBusinessModel model);
        Task<Account> Update(int id, AccountUpdateModel model);
        Task<Account> Delete(int id);
        Task<DynamicModelsResponse<AccountViewModel>> GetAll(AccountViewModel filter, string[] fields, int page, int size);
        Task<AccountViewModel> GetById(int id);
        Task<int> CheckCurrentPassword(int id, string currentPassword);
        Task<Account> UpdateUnban(int id, AccountUnbanModel model);

    }
    public partial class AccountService
    {
        private readonly IConfigurationProvider _mapper;
        public AccountService(IAccountRepository repository, IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
        }

        public TokenModel LoginAsync(LoginModel signInModel)
        {
            var result = Get(x => x.Username == signInModel.Username && x.Password == signInModel.Password).FirstOrDefault();
            if (result == null)
                return null;
            var newToken = GenerateToken(signInModel);
            var expires = new JwtSecurityTokenHandler().ReadToken(newToken).ValidTo;
            return new TokenModel
            {
                Id = result.Id,
                Username = result.Username,
                RoleId = result.RoleId,
                Token = newToken,
                TokenType = "Bearer",
                Expires = expires
            };
        }

        public string GenerateToken(LoginModel user)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("Username", user.Username),
            };

            var securitykey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(RevoCoreConstants.SECRECT_KEY));
            var credentials = new SigningCredentials(securitykey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(RevoCoreConstants.ISSUE_KEY, RevoCoreConstants.ISSUE_KEY, claims,
                   expires: DateTime.Now.AddDays(1),
                   signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<AccountModel> CreateAccount(AccountModel model)
        {
            if (Get(x => x.Username == model.Username).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Username is exsited");
            var entity = _mapper.CreateMapper().Map<Account>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<AccountCreateBusinessModel> CreateAccountBusiness(AccountCreateBusinessModel model)
        {
            if (Get(x => x.Username == model.Username).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Username is exsited");
            var entity = _mapper.CreateMapper().Map<Account>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<Account> Update(int id, AccountUpdateModel model)
        {
            var entity = await GetAsyn(id);
            var password = Get(x => x.Id == id && x.Password == model.CurrentPassword).FirstOrDefault();
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            if (model.Id != id || password == null)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Current password is wrong");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<Account> Delete(int id)
        {
            var entity = Get(id);
            if (entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<AccountViewModel>> GetAll(AccountViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<AccountViewModel>(_mapper)
                .DynamicFilter(model)
                .Select<AccountViewModel>(AccountViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<AccountViewModel>())
                .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<AccountViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        public async Task<AccountViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id && x.Active == true).ProjectTo<AccountViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<int> CheckCurrentPassword(int id, string currentPassword)
        {
            int flag = -1;
            var check = await Get(x => x.Id == id && x.Password == currentPassword).FirstOrDefaultAsync();
            if (check != null)
                flag = 0;
            else
                flag = 1;
            return flag;
        }
        public async Task<Account> UpdateUnban(int id, AccountUnbanModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }
    }
}