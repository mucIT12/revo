﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface ICampaignApplyService
    {
        Task<CampaignApplyModel> CreateCampaignApply(CampaignApplyModel model);
        Task<CampaignApply> UpdateAcceptStatus(int id, CampaignApplyUpdateAcceptStatusModel model);
        Task<CampaignApply> UpdateCompleteStatus(int id, CampaignApplyUpdateCompleteStatusModel model);
        Task<CampaignApply> Delete(int id);
        Task<CampaignApply> DeleteById(int reviewerId, int campaignId);
        Task<DynamicModelsResponse<CampaignApplyViewModel>> GetAll(CampaignApplyViewModel filter, string[] fields, int page, int size);
        //Task<DynamicModelsResponse<CampaignApplyPostViewModel>> GetAllPost(CampaignApplyPostViewModel filter, string[] fields, int page, int size);
        Task<CampaignApplyViewModel> GetById(int id);
        Task<CampaignApplyViewModel> GetIdCampaignApply(int campaignId, int reviewerId);
        Task<int> CheckStatusCampaignApply(int campaignId, int reviewerId);
        Task<int> Count(int campaignId);
        Task<int> CountStatus(int reviewerId, int status);

    }
    public partial class CampaignApplyService
    {
        private readonly IConfigurationProvider _mapper;
        private readonly IBusinessService _businessService;
        private readonly IReviewerService _reviewerService;
        private readonly IBrandService _brandService;
        private readonly ICampaignService _campaignService;

        public CampaignApplyService(ICampaignApplyRepository repository, IBusinessService businessService, IReviewerService reviewerService, ICampaignService campaignService, IBrandService brandService,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
            _businessService = businessService;
            _reviewerService = reviewerService;
            _campaignService = campaignService;
            _brandService = brandService;
        }

        public async Task<CampaignApplyModel> CreateCampaignApply(CampaignApplyModel model)
        {
            if (Get(x => x.CampaignId == model.CampaignId && x.ReviewerId == model.ReviewerId).Any())
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Reviewer participated in the campaign");
            var entity = _mapper.CreateMapper().Map<CampaignApply>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<CampaignApply> UpdateAcceptStatus(int id, CampaignApplyUpdateAcceptStatusModel model)
        {
            var entity = await GetAsyn(id);
            var campaign = _campaignService.Get(x => x.Id == entity.CampaignId).FirstOrDefault();
            var brand = _brandService.Get(x => x.Id == campaign.BrandId).FirstOrDefault();
            var owner = _businessService.Get(x => x.Id == brand.BusinessId).FirstOrDefault();
            var reviewer = _reviewerService.Get(x => x.Id == entity.ReviewerId).FirstOrDefault();
            string message = "Dear Reviewer, " + reviewer.Name + "<br>" + "You have been accepted to participate campaign: " + campaign.Name + "<br>" + "Date: " + DateTime.Now;
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            await MailUtils.SendMailGoogleSmtp("revosup1@gmail.com", reviewer.Email, owner.Name + " accepted your request to participate in the campaign!", message, "revosup1@gmail.com", "Zz123aa123");
            return updateEntity;
        }
        public async Task<CampaignApply> UpdateCompleteStatus(int id, CampaignApplyUpdateCompleteStatusModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }

        public async Task<CampaignApply> Delete(int id)
        {
            var entity = Get(id);
            var campaign = _campaignService.Get(x => x.Id == entity.CampaignId).FirstOrDefault();
            var brand = _brandService.Get(x => x.Id == campaign.BrandId).FirstOrDefault();
            var owner = _businessService.Get(x => x.Id == brand.BusinessId).FirstOrDefault();
            var reviewer = _reviewerService.Get(x => x.Id == entity.ReviewerId).FirstOrDefault();
            string message = "Dear Reviewer, " + reviewer.Name + "<br>" + "You have been denied participation from campaign: " + campaign.Name + "<br>" + "Date: " + DateTime.Now;
            if (entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            await DeleteAsyn(entity);
            await MailUtils.SendMailGoogleSmtp("revosup1@gmail.com", reviewer.Email, owner.Name + " refused your request to join the campaign!", message, "revosup1@gmail.com", "Zz123aa123");
            return entity;
        }

        public async Task<CampaignApply> DeleteById(int reviewerId, int campaignId)
        {
            var entity = Get(x => x.ReviewerId == reviewerId && x.CampaignId == campaignId).FirstOrDefault();
            //var id = Get(x => x.ReviewerId == reviewerId && x.CampaignId == campaignId).FirstOrDefault().Id;
            //if (_postService.Get(x => x.CampaignApplyId == id && x.Status == (int)PostEnum.Confirm).Any())
            //    throw new ErrorResponse((int)HttpStatusCode.BadRequest, "CampaignApply is complete");
            //if (_postService.Get(x => x.CampaignApplyId == id && x.Status == (int)PostEnum.Pending).Any())
            //    throw new ErrorResponse((int)HttpStatusCode.BadRequest, "CampaignApply is considering for post of reviewer");
            if (entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            await DeleteAsyn(entity);
            return entity;
        }

        public async Task<DynamicModelsResponse<CampaignApplyViewModel>> GetAll(CampaignApplyViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<CampaignApplyViewModel>(_mapper)
                .DynamicFilter(model)
                .Select<CampaignApplyViewModel>(CampaignApplyViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<CampaignApplyViewModel>())
                .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<CampaignApplyViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }

        //public async Task<DynamicModelsResponse<CampaignApplyPostViewModel>> GetAllPost(CampaignApplyPostViewModel model, string[] fields, int page, int size)
        //{
        //    var result = Get().ProjectTo<CampaignApplyPostViewModel>(_mapper)
        //        .DynamicFilter(model)
        //        .Select<CampaignApplyPostViewModel>(CampaignApplyPostViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<CampaignApplyPostViewModel>())
        //        .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
        //    var rs = new DynamicModelsResponse<CampaignApplyPostViewModel>
        //    {
        //        Metadata = new PagingMetadata
        //        {
        //            Page = page,
        //            Size = size,
        //            Total = result.Item1
        //        },
        //        Data = await result.Item2.ToListAsync()
        //    };
        //    return rs;
        //}

        public async Task<CampaignApplyViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id).ProjectTo<CampaignApplyViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<CampaignApplyViewModel> GetIdCampaignApply(int campaignId, int reviewerId)
        {
            var result = await Get(x => x.CampaignId == campaignId && x.ReviewerId == reviewerId).ProjectTo<CampaignApplyViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<int> CheckStatusCampaignApply(int campaignId, int reviewerId)
        {
            int flag = -1;
            var check = await Get(x => x.ReviewerId == reviewerId && x.CampaignId == campaignId).ProjectTo<CampaignApplyViewModel>(_mapper).FirstOrDefaultAsync();
            if (check != null)
                flag = 0;
            else
                flag = 1;
            return flag;
        }

        public async Task<int> Count(int campaignId)
        {
            var result = await Get(x => x.CampaignId == campaignId).CountAsync();
            return result;
        }
        public async Task<int> CountStatus(int reviewerId, int status)
        {
            var result = await Get(x => x.ReviewerId == reviewerId && x.Status == status).CountAsync();
            return result;
        }
    }
}
