﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IReviewerService
    {
        Task<ReviewerModel> CreateReviewer(ReviewerModel model);
        Task<Reviewer> Update(int id, ReviewerUpdateModel model);
        Task<Reviewer> UpdateImage(int id, ReviewerUpdateImageModel model);
        Task<Reviewer> Delete(int id);
        Task<DynamicModelsResponse<ReviewerViewModel>> GetAll(ReviewerViewModel filter, string[] fields, int page, int size);
        Task<ReviewerViewModel> GetById(int id);
        Task<ReviewerViewModel> GetByAccountId(int accountId);
    }
    public partial class ReviewerService
    {
        private readonly IConfigurationProvider _mapper;
        private readonly IAccountService _accountService;
        public ReviewerService(IReviewerRepository repository, IAccountService accountService,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
            _accountService = accountService;
        }
        public async Task<ReviewerModel> CreateReviewer(ReviewerModel model)
        {
            var entity = _mapper.CreateMapper().Map<Reviewer>(model);
            await CreateAsyn(entity);
            return model;
        }
        public async Task<Reviewer> Update(int id, ReviewerUpdateModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }
        public async Task<Reviewer> UpdateImage(int id, ReviewerUpdateImageModel model)
        {
            var entity = await GetAsyn(id);
            if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            return updateEntity;
        }
        public async Task<Reviewer> Delete(int id)
        {
            var entity = Get(id);
            //if (entity == null || entity.Active == false)
            //    throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            //entity.Active = false;
            await UpdateAsyn(entity);
            return entity;
        }
        public async Task<DynamicModelsResponse<ReviewerViewModel>> GetAll(ReviewerViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<ReviewerViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<ReviewerViewModel>(ReviewerViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<ReviewerViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<ReviewerViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }
        public async Task<ReviewerViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id).ProjectTo<ReviewerViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }

        public async Task<ReviewerViewModel> GetByAccountId(int accountId)
        {
            var result = await Get(x => x.AccountId == accountId).ProjectTo<ReviewerViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }
    }
}
