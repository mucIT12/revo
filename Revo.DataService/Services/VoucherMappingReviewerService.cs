﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IVoucherMappingReviewerService
    {
        Task<VoucherMappingReviewerModel> CreateVoucherMappingReviewer(VoucherMappingReviewerModel model);
        Task<DynamicModelsResponse<VoucherMappingReviewerViewModel>> GetAll(VoucherMappingReviewerViewModel filter, string[] fields, int page, int size);
    }
    public partial class VoucherMappingReviewerService
    {
        private readonly IConfigurationProvider _mapper;
        private readonly ICampaignApplyService _campaignApplyService;
        public VoucherMappingReviewerService(IVoucherMappingReviewerRepository repository, ICampaignApplyService campaignApplyService,
            IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
            _campaignApplyService = campaignApplyService;
        }

        public async Task<VoucherMappingReviewerModel> CreateVoucherMappingReviewer(VoucherMappingReviewerModel model)
        {
            var entity = _mapper.CreateMapper().Map<VoucherMappingReviewer>(model);
            await CreateAsyn(entity);
            return model;
        }

        public async Task<DynamicModelsResponse<VoucherMappingReviewerViewModel>> GetAll(VoucherMappingReviewerViewModel model, string[] fields, int page, int size)
        {
            var result = Get().ProjectTo<VoucherMappingReviewerViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<VoucherMappingReviewerViewModel>(VoucherMappingReviewerViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<VoucherMappingReviewerViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<VoucherMappingReviewerViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }
    }
}
