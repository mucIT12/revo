﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.Core.Custom;
using Revo.Core.Utilities;
using Revo.DataService.Commons;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.Repositories;
using Revo.DataService.Response;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Revo.DataService.Services
{
    public partial interface IPostService
    {
        Task<PostModel> CreatePost(PostModel model);
        Task<Post> Update(int id, PostUpdateModel model);
        Task<Post> Delete(int id);
        Task<DynamicModelsResponse<PostViewModel>> GetAll(PostViewModel filter, string[] fields, int page, int size);
        Task<PostViewModel> GetById(int id);
    }
    public partial class PostService
    {
        private readonly IConfigurationProvider _mapper;
        private readonly IBusinessService _businessService;
        private readonly IReviewerService _reviewerService;
        private readonly IBrandService _brandService;
        private readonly ICampaignService _campaignService;
        private readonly ICampaignApplyService _campaignApplyService;
        private readonly IVoucherService _voucherService;
        public PostService(IPostRepository repository, IBusinessService businessService, IReviewerService reviewerService, IBrandService brandService, 
            ICampaignService campaignService, ICampaignApplyService campaignApplyService, IVoucherService voucherService,
        IUnitOfWork unitOfWork, IMapper mapper = null) : base(unitOfWork, repository)
        {
            _mapper = mapper.ConfigurationProvider;
            _businessService = businessService;
            _reviewerService = reviewerService;
            _campaignService = campaignService;
            _campaignApplyService = campaignApplyService;
            _brandService = brandService;
            _voucherService = voucherService;
        }
        public async Task<PostModel> CreatePost(PostModel model)
        {
            var entity = _mapper.CreateMapper().Map<Post>(model);
            await CreateAsyn(entity);
            return model;
        }
        public async Task<Post> Update(int id, PostUpdateModel model)
        {
            var entity = await GetAsyn(id);
            var reviewer = _reviewerService.Get(x => x.Id == entity.Author).FirstOrDefault();
            var campaingApply = _campaignApplyService.Get(x => x.Id == entity.CampaignApplyId).FirstOrDefault();
            var campaign = _campaignService.Get(x => x.Id == campaingApply.CampaignId).FirstOrDefault();
            var brand = _brandService.Get(x => x.Id == campaign.BrandId).FirstOrDefault();
            var owner = _businessService.Get(x => x.Id == brand.BusinessId).FirstOrDefault();
            var voucher = _voucherService.Get(x => x.Id == campaign.VoucherId).FirstOrDefault();

            string message = "Dear Reviewer, " + reviewer.Name + "<br>" + 
                "Your post has been accepted in campaign: " + campaign.Name + "<br>" +
                "You did a great job this time" + "<br>" +
                "Your voucher: " + voucher.Code +
                "Date: " + DateTime.Now;
            if (entity == null || entity.Active == false)
                if (model.Id != id)
                throw new ErrorResponse((int)HttpStatusCode.BadRequest, "Id not matched");
            if (model.Id != id || entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            var updateEntity = _mapper.CreateMapper().Map(model, entity);
            await UpdateAsyn(updateEntity);
            await MailUtils.SendMailGoogleSmtp("revosup1@gmail.com", reviewer.Email, "The post has been accepted from " + owner.Name + "!", message, "revosup1@gmail.com", "Zz123aa123");
            return updateEntity;
        }
        public async Task<Post> Delete(int id)
        {
            var entity = await GetAsyn(id);
            var reviewer = _reviewerService.Get(x => x.Id == entity.Author).FirstOrDefault();
            var campaingApply = _campaignApplyService.Get(x => x.Id == entity.CampaignApplyId).FirstOrDefault();
            var campaign = _campaignService.Get(x => x.Id == campaingApply.CampaignId).FirstOrDefault();
            var brand = _brandService.Get(x => x.Id == campaign.BrandId).FirstOrDefault();
            var owner = _businessService.Get(x => x.Id == brand.BusinessId).FirstOrDefault();

            string message = "Dear Reviewer, " + reviewer.Name + "<br>" + "Your post has not been accepted in campaign: " + campaign.Name + "<br>" +
                "Please check your post again!" + "<br>" +
                "Date: " + DateTime.Now;
            if (entity == null || entity.Active == false)
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Not found");
            entity.Active = false;
            entity.Status = (int)PostEnum.Cancel;
            await UpdateAsyn(entity);
            await MailUtils.SendMailGoogleSmtp("revosup1@gmail.com", reviewer.Email, "The post has been rejected from " + owner.Name + "!", message, "revosup1@gmail.com", "Zz123aa123");
            return entity;
        }

        public async Task<DynamicModelsResponse<PostViewModel>> GetAll(PostViewModel model, string[] fields, int page, int size)
        {
            var result = Get(x => x.Active == true).ProjectTo<PostViewModel>(_mapper)
                  .DynamicFilter(model)
                  .Select<PostViewModel>(PostViewModel.Fields.Union(fields).ToArray().ToDynamicSelector<PostViewModel>())
                  .PagingIQueryable(page, size, CommonConstants.LimitPaging, CommonConstants.DefaultPaging);
            var rs = new DynamicModelsResponse<PostViewModel>
            {
                Metadata = new PagingMetadata
                {
                    Page = page,
                    Size = size,
                    Total = result.Item1
                },
                Data = await result.Item2.ToListAsync()
            };
            return rs;
        }
        public async Task<PostViewModel> GetById(int id)
        {
            var result = await Get(x => x.Id == id && x.Active == true).ProjectTo<PostViewModel>(_mapper).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new ErrorResponse((int)HttpStatusCode.NotFound, "Can not find");
            }
            return result;
        }



    }
}
