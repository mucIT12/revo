/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IReviewerMappingBrandService:IBaseService<ReviewerMappingBrand>
    {
    }
    public partial class ReviewerMappingBrandService:BaseService<ReviewerMappingBrand>,IReviewerMappingBrandService
    {
        public ReviewerMappingBrandService(IUnitOfWork unitOfWork,IReviewerMappingBrandRepository repository):base(unitOfWork,repository){}
    }
}
