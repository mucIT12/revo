/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IPostService:IBaseService<Post>
    {
    }
    public partial class PostService:BaseService<Post>,IPostService
    {
        public PostService(IUnitOfWork unitOfWork,IPostRepository repository):base(unitOfWork,repository){}
    }
}
