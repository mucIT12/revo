/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IAccountService:IBaseService<Account>
    {
    }
    public partial class AccountService:BaseService<Account>,IAccountService
    {
        public AccountService(IUnitOfWork unitOfWork,IAccountRepository repository):base(unitOfWork,repository){}
    }
}
