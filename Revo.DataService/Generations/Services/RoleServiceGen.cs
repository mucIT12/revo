/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IRoleService:IBaseService<Role>
    {
    }
    public partial class RoleService:BaseService<Role>,IRoleService
    {
        public RoleService(IUnitOfWork unitOfWork,IRoleRepository repository):base(unitOfWork,repository){}
    }
}
