/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IIndustryTypeService:IBaseService<IndustryType>
    {
    }
    public partial class IndustryTypeService:BaseService<IndustryType>,IIndustryTypeService
    {
        public IndustryTypeService(IUnitOfWork unitOfWork,IIndustryTypeRepository repository):base(unitOfWork,repository){}
    }
}
