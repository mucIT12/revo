/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IBusinessService:IBaseService<Business>
    {
    }
    public partial class BusinessService:BaseService<Business>,IBusinessService
    {
        public BusinessService(IUnitOfWork unitOfWork,IBusinessRepository repository):base(unitOfWork,repository){}
    }
}
