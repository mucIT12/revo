/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface ICampaignApplyService:IBaseService<CampaignApply>
    {
    }
    public partial class CampaignApplyService:BaseService<CampaignApply>,ICampaignApplyService
    {
        public CampaignApplyService(IUnitOfWork unitOfWork,ICampaignApplyRepository repository):base(unitOfWork,repository){}
    }
}
