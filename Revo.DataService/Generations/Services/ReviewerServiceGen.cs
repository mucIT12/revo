/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IReviewerService:IBaseService<Reviewer>
    {
    }
    public partial class ReviewerService:BaseService<Reviewer>,IReviewerService
    {
        public ReviewerService(IUnitOfWork unitOfWork,IReviewerRepository repository):base(unitOfWork,repository){}
    }
}
