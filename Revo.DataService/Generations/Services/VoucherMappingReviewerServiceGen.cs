/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IVoucherMappingReviewerService:IBaseService<VoucherMappingReviewer>
    {
    }
    public partial class VoucherMappingReviewerService:BaseService<VoucherMappingReviewer>,IVoucherMappingReviewerService
    {
        public VoucherMappingReviewerService(IUnitOfWork unitOfWork,IVoucherMappingReviewerRepository repository):base(unitOfWork,repository){}
    }
}
