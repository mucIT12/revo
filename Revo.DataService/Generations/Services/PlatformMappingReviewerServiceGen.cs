/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IPlatformMappingReviewerService:IBaseService<PlatformMappingReviewer>
    {
    }
    public partial class PlatformMappingReviewerService:BaseService<PlatformMappingReviewer>,IPlatformMappingReviewerService
    {
        public PlatformMappingReviewerService(IUnitOfWork unitOfWork,IPlatformMappingReviewerRepository repository):base(unitOfWork,repository){}
    }
}
