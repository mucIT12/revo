/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IPlatformService:IBaseService<Platform>
    {
    }
    public partial class PlatformService:BaseService<Platform>,IPlatformService
    {
        public PlatformService(IUnitOfWork unitOfWork,IPlatformRepository repository):base(unitOfWork,repository){}
    }
}
