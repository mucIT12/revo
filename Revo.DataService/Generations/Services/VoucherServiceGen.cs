/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IVoucherService:IBaseService<Voucher>
    {
    }
    public partial class VoucherService:BaseService<Voucher>,IVoucherService
    {
        public VoucherService(IUnitOfWork unitOfWork,IVoucherRepository repository):base(unitOfWork,repository){}
    }
}
