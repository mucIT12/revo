/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface ICampaignService:IBaseService<Campaign>
    {
    }
    public partial class CampaignService:BaseService<Campaign>,ICampaignService
    {
        public CampaignService(IUnitOfWork unitOfWork,ICampaignRepository repository):base(unitOfWork,repository){}
    }
}
