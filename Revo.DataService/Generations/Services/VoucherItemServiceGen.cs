/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IVoucherItemService:IBaseService<VoucherItem>
    {
    }
    public partial class VoucherItemService:BaseService<VoucherItem>,IVoucherItemService
    {
        public VoucherItemService(IUnitOfWork unitOfWork,IVoucherItemRepository repository):base(unitOfWork,repository){}
    }
}
