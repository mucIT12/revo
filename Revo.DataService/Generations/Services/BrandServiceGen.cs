/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IBrandService:IBaseService<Brand>
    {
    }
    public partial class BrandService:BaseService<Brand>,IBrandService
    {
        public BrandService(IUnitOfWork unitOfWork,IBrandRepository repository):base(unitOfWork,repository){}
    }
}
