/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////
namespace Revo.DataService.Services
{
    using Revo.Core.BaseConnect;
    using Revo.DataService.Models;
    using Revo.DataService.Repositories;
    public partial interface IIndustryService:IBaseService<Industry>
    {
    }
    public partial class IndustryService:BaseService<Industry>,IIndustryService
    {
        public IndustryService(IUnitOfWork unitOfWork,IIndustryRepository repository):base(unitOfWork,repository){}
    }
}
