/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IAccountRepository :IBaseRepository<Account>
    {
    }
    public partial class AccountRepository :BaseRepository<Account>, IAccountRepository
    {
         public AccountRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

