/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface ICampaignApplyRepository :IBaseRepository<CampaignApply>
    {
    }
    public partial class CampaignApplyRepository :BaseRepository<CampaignApply>, ICampaignApplyRepository
    {
         public CampaignApplyRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

