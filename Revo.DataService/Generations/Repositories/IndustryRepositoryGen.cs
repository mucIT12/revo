/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IIndustryRepository :IBaseRepository<Industry>
    {
    }
    public partial class IndustryRepository :BaseRepository<Industry>, IIndustryRepository
    {
         public IndustryRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

