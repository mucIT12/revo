/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IPostRepository :IBaseRepository<Post>
    {
    }
    public partial class PostRepository :BaseRepository<Post>, IPostRepository
    {
         public PostRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

