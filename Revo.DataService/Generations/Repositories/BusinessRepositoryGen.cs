/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IBusinessRepository :IBaseRepository<Business>
    {
    }
    public partial class BusinessRepository :BaseRepository<Business>, IBusinessRepository
    {
         public BusinessRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

