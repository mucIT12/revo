/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IPlatformMappingReviewerRepository :IBaseRepository<PlatformMappingReviewer>
    {
    }
    public partial class PlatformMappingReviewerRepository :BaseRepository<PlatformMappingReviewer>, IPlatformMappingReviewerRepository
    {
         public PlatformMappingReviewerRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

