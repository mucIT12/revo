/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IVoucherRepository :IBaseRepository<Voucher>
    {
    }
    public partial class VoucherRepository :BaseRepository<Voucher>, IVoucherRepository
    {
         public VoucherRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

