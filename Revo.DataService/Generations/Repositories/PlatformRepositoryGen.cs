/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IPlatformRepository :IBaseRepository<Platform>
    {
    }
    public partial class PlatformRepository :BaseRepository<Platform>, IPlatformRepository
    {
         public PlatformRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

