/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IReviewerMappingBrandRepository :IBaseRepository<ReviewerMappingBrand>
    {
    }
    public partial class ReviewerMappingBrandRepository :BaseRepository<ReviewerMappingBrand>, IReviewerMappingBrandRepository
    {
         public ReviewerMappingBrandRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

