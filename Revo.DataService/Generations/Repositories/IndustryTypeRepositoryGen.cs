/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IIndustryTypeRepository :IBaseRepository<IndustryType>
    {
    }
    public partial class IndustryTypeRepository :BaseRepository<IndustryType>, IIndustryTypeRepository
    {
         public IndustryTypeRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

