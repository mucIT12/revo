/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface ICampaignRepository :IBaseRepository<Campaign>
    {
    }
    public partial class CampaignRepository :BaseRepository<Campaign>, ICampaignRepository
    {
         public CampaignRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

