/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IReviewerRepository :IBaseRepository<Reviewer>
    {
    }
    public partial class ReviewerRepository :BaseRepository<Reviewer>, IReviewerRepository
    {
         public ReviewerRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

