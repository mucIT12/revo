/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IVoucherMappingReviewerRepository :IBaseRepository<VoucherMappingReviewer>
    {
    }
    public partial class VoucherMappingReviewerRepository :BaseRepository<VoucherMappingReviewer>, IVoucherMappingReviewerRepository
    {
         public VoucherMappingReviewerRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

