/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IVoucherItemRepository :IBaseRepository<VoucherItem>
    {
    }
    public partial class VoucherItemRepository :BaseRepository<VoucherItem>, IVoucherItemRepository
    {
         public VoucherItemRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

