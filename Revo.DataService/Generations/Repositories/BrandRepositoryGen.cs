/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
using Revo.DataService.Models;
namespace Revo.DataService.Repositories
{
    public partial interface IBrandRepository :IBaseRepository<Brand>
    {
    }
    public partial class BrandRepository :BaseRepository<Brand>, IBrandRepository
    {
         public BrandRepository(DbContext dbContext) : base(dbContext)
         {
         }
    }
}

