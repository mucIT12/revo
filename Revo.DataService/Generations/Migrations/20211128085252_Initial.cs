﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Revo.DataService.Generations.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Industry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    Hashtag = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Active = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Platform",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    Active = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Platform", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleName = table.Column<string>(maxLength: 50, nullable: true),
                    Active = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IndustryType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    Hashtag = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    IndustryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndustryType", x => x.Id);
                    table.ForeignKey(
                        name: "FK__IndustryT__Indus__1ED998B2",
                        column: x => x.IndustryId,
                        principalTable: "Industry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    Password = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    RoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Account__RoleId__1273C1CD",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Business",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Hashtag = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 50, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 15, nullable: true),
                    AccountId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Business__Accoun__164452B1",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ShortName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Gender = table.Column<string>(maxLength: 50, nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime", nullable: true),
                    Address = table.Column<string>(maxLength: 150, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    WorkHistory = table.Column<string>(maxLength: 500, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 15, nullable: true),
                    AccountId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Reviewer__Accoun__1A14E395",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Brand",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Hashtag = table.Column<string>(maxLength: 50, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    BusinessId = table.Column<int>(nullable: true),
                    IndustryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brand", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Brand__BusinessI__21B6055D",
                        column: x => x.BusinessId,
                        principalTable: "Business",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Brand__IndustryI__22AA2996",
                        column: x => x.IndustryId,
                        principalTable: "Industry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlatformMappingReviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Linkpf = table.Column<string>(maxLength: 255, nullable: true),
                    PlatformId = table.Column<int>(nullable: true),
                    ReviewerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformMappingReviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK__PlatformM__Platf__34C8D9D1",
                        column: x => x.PlatformId,
                        principalTable: "Platform",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__PlatformM__Revie__35BCFE0A",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReviewerMappingBrand",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReviewerId = table.Column<int>(nullable: true),
                    BrandId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewerMappingBrand", x => x.Id);
                    table.ForeignKey(
                        name: "FK__ReviewerM__Brand__267ABA7A",
                        column: x => x.BrandId,
                        principalTable: "Brand",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ReviewerM__Revie__25869641",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Voucher",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    Code = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    Discount = table.Column<int>(nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    BrandId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Voucher", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Voucher__BrandId__29572725",
                        column: x => x.BrandId,
                        principalTable: "Brand",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Campaign",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 2000, nullable: true),
                    Image = table.Column<string>(maxLength: 2000, nullable: true),
                    CampaignDecription = table.Column<string>(maxLength: 2000, nullable: true),
                    RecruitmentStatus = table.Column<int>(nullable: true),
                    RecruitmentDate = table.Column<DateTime>(type: "date", nullable: true),
                    Applied = table.Column<int>(nullable: true),
                    Slot = table.Column<int>(nullable: true),
                    Hashtag = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    LinkBrand = table.Column<string>(maxLength: 2000, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ConditionApply = table.Column<string>(maxLength: 2000, nullable: true),
                    WorkDescription = table.Column<string>(maxLength: 2000, nullable: true),
                    CampaignStatus = table.Column<int>(nullable: true),
                    FeaturedStatus = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    BrandId = table.Column<int>(nullable: true),
                    PlatformId = table.Column<int>(nullable: true),
                    VoucherId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Campaign__BrandI__403A8C7D",
                        column: x => x.BrandId,
                        principalTable: "Brand",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Campaign__Platfo__412EB0B6",
                        column: x => x.PlatformId,
                        principalTable: "Platform",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Campaign__Vouche__4222D4EF",
                        column: x => x.VoucherId,
                        principalTable: "Voucher",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VoucherMappingReviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiveDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReviewerId = table.Column<int>(nullable: true),
                    VoucherId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoucherMappingReviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK__VoucherMa__Revie__3F466844",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__VoucherMa__Vouch__403A8C7D",
                        column: x => x.VoucherId,
                        principalTable: "Voucher",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampaignApply",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplyDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<int>(nullable: true),
                    ReviewerId = table.Column<int>(nullable: true),
                    CampaignId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignApply", x => x.Id);
                    table.ForeignKey(
                        name: "FK__CampaignA__Campa__45F365D3",
                        column: x => x.CampaignId,
                        principalTable: "Campaign",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__CampaignA__Revie__44FF419A",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Post",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 500, nullable: true),
                    Url = table.Column<string>(maxLength: 255, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    ApplyDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ComfirmDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Author = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    CampaignApplyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Post", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Post__CampaignAp__4CA06362",
                        column: x => x.CampaignApplyId,
                        principalTable: "CampaignApply",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Account_RoleId",
                table: "Account",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Brand_BusinessId",
                table: "Brand",
                column: "BusinessId");

            migrationBuilder.CreateIndex(
                name: "IX_Brand_IndustryId",
                table: "Brand",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "UQ__Business__349DA5A76FC60658",
                table: "Business",
                column: "AccountId",
                unique: true,
                filter: "[AccountId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Campaign_BrandId",
                table: "Campaign",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaign_PlatformId",
                table: "Campaign",
                column: "PlatformId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaign_VoucherId",
                table: "Campaign",
                column: "VoucherId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignApply_CampaignId",
                table: "CampaignApply",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignApply_ReviewerId",
                table: "CampaignApply",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_IndustryType_IndustryId",
                table: "IndustryType",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformMappingReviewer_PlatformId",
                table: "PlatformMappingReviewer",
                column: "PlatformId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformMappingReviewer_ReviewerId",
                table: "PlatformMappingReviewer",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_CampaignApplyId",
                table: "Post",
                column: "CampaignApplyId");

            migrationBuilder.CreateIndex(
                name: "UQ__Reviewer__349DA5A73EDB1725",
                table: "Reviewer",
                column: "AccountId",
                unique: true,
                filter: "[AccountId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewerMappingBrand_BrandId",
                table: "ReviewerMappingBrand",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewerMappingBrand_ReviewerId",
                table: "ReviewerMappingBrand",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_Voucher_BrandId",
                table: "Voucher",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_VoucherMappingReviewer_ReviewerId",
                table: "VoucherMappingReviewer",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_VoucherMappingReviewer_VoucherId",
                table: "VoucherMappingReviewer",
                column: "VoucherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IndustryType");

            migrationBuilder.DropTable(
                name: "PlatformMappingReviewer");

            migrationBuilder.DropTable(
                name: "Post");

            migrationBuilder.DropTable(
                name: "ReviewerMappingBrand");

            migrationBuilder.DropTable(
                name: "VoucherMappingReviewer");

            migrationBuilder.DropTable(
                name: "CampaignApply");

            migrationBuilder.DropTable(
                name: "Campaign");

            migrationBuilder.DropTable(
                name: "Reviewer");

            migrationBuilder.DropTable(
                name: "Platform");

            migrationBuilder.DropTable(
                name: "Voucher");

            migrationBuilder.DropTable(
                name: "Brand");

            migrationBuilder.DropTable(
                name: "Business");

            migrationBuilder.DropTable(
                name: "Industry");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}
