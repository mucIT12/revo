
/////////////////////////////////////////////////////////////////
//
//              AUTO-GENERATED
//
/////////////////////////////////////////////////////////////////

using Revo.DataService.Models;
using Microsoft.Extensions.DependencyInjection;
using Revo.DataService.Services;
using Revo.DataService.Repositories;
using Microsoft.EntityFrameworkCore;
using Revo.Core.BaseConnect;
namespace Revo.DataService.Commons
{
    public static class DependencyInjectionResolverGen
    {
        public static void InitializerDI(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<DbContext, RevoContext>();
        
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAccountRepository, AccountRepository>();
        
            services.AddScoped<IBrandService, BrandService>();
            services.AddScoped<IBrandRepository, BrandRepository>();
        
            services.AddScoped<IBusinessService, BusinessService>();
            services.AddScoped<IBusinessRepository, BusinessRepository>();
        
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();
        
            services.AddScoped<ICampaignApplyService, CampaignApplyService>();
            services.AddScoped<ICampaignApplyRepository, CampaignApplyRepository>();
        
            services.AddScoped<IIndustryService, IndustryService>();
            services.AddScoped<IIndustryRepository, IndustryRepository>();
        
            services.AddScoped<IIndustryTypeService, IndustryTypeService>();
            services.AddScoped<IIndustryTypeRepository, IndustryTypeRepository>();
        
            services.AddScoped<IPlatformService, PlatformService>();
            services.AddScoped<IPlatformRepository, PlatformRepository>();
        
            services.AddScoped<IPlatformMappingReviewerService, PlatformMappingReviewerService>();
            services.AddScoped<IPlatformMappingReviewerRepository, PlatformMappingReviewerRepository>();
        
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IPostRepository, PostRepository>();
        
            services.AddScoped<IReviewerService, ReviewerService>();
            services.AddScoped<IReviewerRepository, ReviewerRepository>();
        
            services.AddScoped<IReviewerMappingBrandService, ReviewerMappingBrandService>();
            services.AddScoped<IReviewerMappingBrandRepository, ReviewerMappingBrandRepository>();
        
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IRoleRepository, RoleRepository>();
        
            services.AddScoped<IVoucherService, VoucherService>();
            services.AddScoped<IVoucherRepository, VoucherRepository>();
        
            services.AddScoped<IVoucherItemService, VoucherItemService>();
            services.AddScoped<IVoucherItemRepository, VoucherItemRepository>();
        
            services.AddScoped<IVoucherMappingReviewerService, VoucherMappingReviewerService>();
            services.AddScoped<IVoucherMappingReviewerRepository, VoucherMappingReviewerRepository>();
        }
    }
}
