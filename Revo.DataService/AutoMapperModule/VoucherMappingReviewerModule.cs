﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class VoucherMappingReviewerModule
    {
        public static void ConfigVoucherMappingReviewerModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<VoucherMappingReviewer, VoucherMappingReviewerViewModel>();
            mc.CreateMap<VoucherMappingReviewerViewModel, VoucherMappingReviewer>();

            mc.CreateMap<VoucherMappingReviewer, VoucherMappingReviewerModel>();
            mc.CreateMap<VoucherMappingReviewerModel, VoucherMappingReviewer>()
                .ForMember(des => des.ReceiveDate, opt => opt.MapFrom(src => DateTime.Now));

        }
    }
}
