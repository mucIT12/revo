﻿using AutoMapper;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class VoucherModule
    {
        public static void ConfigVoucherModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Voucher, VoucherModel>();
            mc.CreateMap<VoucherModel, Voucher>()
                .ForMember(des => des.StartDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)VoucherEnum.Availability))
                .ForMember(des => des.Active, opt => opt.MapFrom(src => 1));
            mc.CreateMap<Voucher, VoucherViewModel>();
            mc.CreateMap<VoucherViewModel, Voucher>();

            mc.CreateMap<Voucher, VoucherMappingCampaignModel>();
            mc.CreateMap<VoucherMappingCampaignModel, Voucher>();

            mc.CreateMap<Voucher, VoucherMappingModel>();
            mc.CreateMap<VoucherMappingModel, Voucher>();
        }
    }
}
