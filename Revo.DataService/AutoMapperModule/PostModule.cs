﻿using AutoMapper;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class PostModule
    {
        public static void ConfigPostModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Post, PostModel>();
            mc.CreateMap<PostModel, Post>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true))
                .ForMember(des => des.ApplyDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)PostEnum.Pending));

            mc.CreateMap<Post, PostViewModel>();
            mc.CreateMap<PostViewModel, Post>();

            mc.CreateMap<Post, PostMappingCampaign>();
            mc.CreateMap<PostMappingCampaign, Post>();

            mc.CreateMap<Post, PostUpdateModel>();
            mc.CreateMap<PostUpdateModel, Post>()
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)PostEnum.Confirm));

        }
    }
}
