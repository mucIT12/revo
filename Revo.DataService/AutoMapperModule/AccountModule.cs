﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class AccountModule
    {
        public static void ConfigAccountModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Account, AccountModel>();
            mc.CreateMap<AccountModel, Account>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true))
                .ForMember(des => des.RoleId, opt => opt.MapFrom(src => 1))
                .ForMember(des => des.CreateDate, opt => opt.MapFrom(src => DateTime.Now));
            mc.CreateMap<Account, AccountCreateBusinessModel>();
            mc.CreateMap<AccountCreateBusinessModel, Account>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true))
                .ForMember(des => des.RoleId, opt => opt.MapFrom(src => 2))
                .ForMember(des => des.CreateDate, opt => opt.MapFrom(src => DateTime.Now));
            mc.CreateMap<Account, AccountViewModel>();
            mc.CreateMap<AccountViewModel, Account>();

            mc.CreateMap<Account, LoginModel>();
            mc.CreateMap<LoginModel, Account>();

            mc.CreateMap<Account, TokenModel>();
            mc.CreateMap<TokenModel, Account>();

            mc.CreateMap<Account, AccountUpdateModel>();
            mc.CreateMap<AccountUpdateModel, Account>();

            mc.CreateMap<Role, RoleMappingAccount>();
            mc.CreateMap<RoleMappingAccount, Role>();

            mc.CreateMap<Account, AccountUnbanModel>();
            mc.CreateMap<AccountUnbanModel, Account>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true));
        }
    }
}
