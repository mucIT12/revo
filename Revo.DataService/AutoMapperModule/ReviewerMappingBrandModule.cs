﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class ReviewerMappingBrandModule
    {
        public static void ConfigReviewerMappingBrandModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<ReviewerMappingBrand, ReviewerMappingBrandModel>();
            mc.CreateMap<ReviewerMappingBrandModel, ReviewerMappingBrand>();

            mc.CreateMap<ReviewerMappingBrand, ReviewerMappingBrandViewModel>()
                .ForMember(des => des.Reviewerid, opt => opt.MapFrom(src => src.ReviewerId))
                .ForMember(des => des.Brandid, opt => opt.MapFrom(src => src.BrandId));
            mc.CreateMap<ReviewerMappingBrandViewModel, ReviewerMappingBrand>()
                .ForMember(des => des.ReviewerId, opt => opt.MapFrom(src => src.Reviewerid))
                .ForMember(des => des.BrandId, opt => opt.MapFrom(src => src.Brandid));
            
            mc.CreateMap<ReviewerMappingBrand, ReviewerMappingBrandCheckModel>();
            mc.CreateMap<ReviewerMappingBrandCheckModel, ReviewerMappingBrand>();
        }
    }
}
