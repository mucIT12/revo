﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class PlatformModule
    {
        public static void ConfigPlatformModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Platform, PlatformModel>();
            mc.CreateMap<PlatformModel, Platform>();
                //field gen

            mc.CreateMap<Platform, PlatformViewModel>();
            mc.CreateMap<PlatformViewModel, Platform>();

            mc.CreateMap<Platform, PlatformMappingCampaignModel>();
            mc.CreateMap<PlatformMappingCampaignModel, Platform>();
        }
    }
}
