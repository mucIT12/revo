﻿using AutoMapper;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class CampaignApplyModule
    {
        public static void ConfigCampaignApplyModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<CampaignApply, CampaignApplyModel>();
            mc.CreateMap<CampaignApplyModel, CampaignApply>()
                .ForMember(des => des.ApplyDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)CampaignApplyEnum.Applying));

            mc.CreateMap<CampaignApply, CampaignApplyViewModel>();
            mc.CreateMap<CampaignApplyViewModel, CampaignApply>();
            
            mc.CreateMap<CampaignApply, CampaignApplyUpdateAcceptStatusModel>();
            mc.CreateMap<CampaignApplyUpdateAcceptStatusModel, CampaignApply>()
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)CampaignApplyEnum.InChange));

            mc.CreateMap<CampaignApply, CampaignApplyUpdateCompleteStatusModel>();
            mc.CreateMap<CampaignApplyUpdateCompleteStatusModel, CampaignApply>()
                .ForMember(des => des.Status, opt => opt.MapFrom(src => (int)CampaignApplyEnum.Complete));

            mc.CreateMap<CampaignApply, CampaignApplyMappingPost>();
            mc.CreateMap<CampaignApplyMappingPost, CampaignApply>();

            mc.CreateMap<CampaignApply, CampaignApplyPostViewModel>()
                .ForMember(des => des.Reviewerid, opt => opt.MapFrom(src => src.ReviewerId));
            mc.CreateMap<CampaignApplyPostViewModel, CampaignApply>()
                .ForMember(des => des.ReviewerId, opt => opt.MapFrom(src => src.Reviewerid));
            
        }
    }
}
