﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class IndustryModule
    {
        public static void ConfigIndustryModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Industry, IndustryModel>();
            mc.CreateMap<IndustryModel, Industry>()
                //field gen
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true));
            mc.CreateMap<Industry, IndustryViewModel>();
            mc.CreateMap<IndustryViewModel, Industry>();

            mc.CreateMap<Industry, IndustryMappingBrand>();
            mc.CreateMap<IndustryMappingBrand, Industry>();
        }
    }
}
