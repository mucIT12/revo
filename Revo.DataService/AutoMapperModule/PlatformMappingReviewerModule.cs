﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class PlatformMappingReviewerModule
    {
        public static void ConfigPlatformMappingReviewerModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<PlatformMappingReviewer, PlatformMappingReviewerModel>();
            mc.CreateMap<PlatformMappingReviewerModel, PlatformMappingReviewer>();

            mc.CreateMap<PlatformMappingReviewer, PlatformMappingReviewerViewModel>()
                .ForMember(des => des.Reviewerid, opt => opt.MapFrom(src => src.ReviewerId))
                .ForMember(des => des.Platformid, opt => opt.MapFrom(src => src.PlatformId));
            mc.CreateMap<PlatformMappingReviewerViewModel, PlatformMappingReviewer>()
                .ForMember(des => des.ReviewerId, opt => opt.MapFrom(src => src.Reviewerid))
                .ForMember(des => des.PlatformId, opt => opt.MapFrom(src => src.Platformid));

            mc.CreateMap<PlatformMappingReviewer, PlatformMappingReviewerUpdateModel>();
            mc.CreateMap<PlatformMappingReviewerUpdateModel, PlatformMappingReviewer>();

        }
    }
}
