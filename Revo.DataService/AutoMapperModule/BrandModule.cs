﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class BrandModule
    {
        public static void ConfigBrandModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Brand, BrandModel>();
            mc.CreateMap<BrandModel, Brand>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true));

            mc.CreateMap<Brand, BrandViewModel>()
                .ForMember(des => des.Industryid, opt => opt.MapFrom(src => src.IndustryId))
                .ForMember(des => des.Businessid, opt => opt.MapFrom(src => src.BusinessId));
            mc.CreateMap<BrandViewModel, Brand>()
                .ForMember(des => des.IndustryId, opt => opt.MapFrom(src => src.Industryid))
                .ForMember(des => des.BusinessId, opt => opt.MapFrom(src => src.Businessid));

            mc.CreateMap<Brand, BrandMappingCampaignModel>();
            mc.CreateMap<BrandMappingCampaignModel, Brand>();

            mc.CreateMap<Brand, BrandUpdateModel>();
            mc.CreateMap<BrandUpdateModel, Brand>();

            mc.CreateMap<Brand, BrandNameModel>();
            mc.CreateMap<BrandNameModel, Brand>();

        }
    }
}
