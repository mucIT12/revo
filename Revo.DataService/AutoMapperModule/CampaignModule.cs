﻿using AutoMapper;
using Revo.DataService.Enums;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class CampaignModule
    {
        public static void ConfigCampaignModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Campaign, CampaignModel>();
            mc.CreateMap<CampaignModel, Campaign>()
                .ForMember(des => des.Active, opt => opt.MapFrom(src => true))
                .ForMember(des => des.RecruitmentStatus, opt => opt.MapFrom(src => (int)RecruitmentEnum.Recruiting))
                .ForMember(des => des.Applied, opt => opt.MapFrom(src => 0))
                .ForMember(des => des.CampaignStatus, opt => opt.MapFrom(src => (int)CampaignEnum.InProgress))
                .ForMember(des => des.FeaturedStatus, opt => opt.MapFrom(src => (int)FeaturedEnum.NewCampaign));

            mc.CreateMap<Campaign, CampaignViewModel>()
                .ForMember(des => des.Featuredstatus, opt => opt.MapFrom(src => src.FeaturedStatus));
            mc.CreateMap<CampaignViewModel, Campaign>()
                .ForMember(des => des.FeaturedStatus, opt => opt.MapFrom(src => src.Featuredstatus));

            mc.CreateMap<Campaign, CampaignViewAllModel>();
            mc.CreateMap<CampaignViewAllModel, Campaign>();

            mc.CreateMap<Campaign, CampaignDetailViewModel>();
            mc.CreateMap<CampaignDetailViewModel, Campaign>();
            
            mc.CreateMap<Campaign, CampaignUpdateModel>();
            mc.CreateMap<CampaignUpdateModel, Campaign>();

            mc.CreateMap<Campaign, CampaignUpdateStatusModel>();
            mc.CreateMap<CampaignUpdateStatusModel, Campaign>()
                .ForMember(des => des.CampaignStatus, opt => opt.MapFrom(src => (int)CampaignEnum.Closed));

            mc.CreateMap<Campaign, CampaignNameModel>();
            mc.CreateMap<CampaignNameModel, Campaign>();

            mc.CreateMap<Campaign, CampaignMappingPost>();
            mc.CreateMap<CampaignMappingPost, Campaign>();

            
        }
    }
}
