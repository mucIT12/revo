﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class BusinessModule
    {
        public static void ConfigBusinessModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Business, BusinessModel>();
            mc.CreateMap<BusinessModel, Business>();

            mc.CreateMap<Business, BusinessViewModel>();
            mc.CreateMap<BusinessViewModel, Business>();

            mc.CreateMap<Business, BusinessMappingBrand>();
            mc.CreateMap<BusinessMappingBrand, Business>();
        }
    }
}
