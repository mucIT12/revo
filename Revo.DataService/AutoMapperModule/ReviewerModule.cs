﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class ReviewerModule
    {
        public static void ConfigReviewerModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Reviewer, ReviewerModel>();
            mc.CreateMap<ReviewerModel, Reviewer>();

            mc.CreateMap<Reviewer, ReviewerViewModel>();
            mc.CreateMap<ReviewerViewModel, Reviewer>();

            mc.CreateMap<Reviewer, ReviewerMappingPost>();
            mc.CreateMap<ReviewerMappingPost, Reviewer>();

            mc.CreateMap<Reviewer, ReviewerUpdateModel>();
            mc.CreateMap<ReviewerUpdateModel, Reviewer>();

            mc.CreateMap<Reviewer, ReviewerUpdateImageModel>();
            mc.CreateMap<ReviewerUpdateImageModel, Reviewer>();

            mc.CreateMap<Reviewer, ReviewerMappingModel>();
            mc.CreateMap<ReviewerMappingModel, Reviewer>();
        }
    }
}
