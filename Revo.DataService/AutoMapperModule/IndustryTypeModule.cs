﻿using AutoMapper;
using Revo.DataService.Models;
using Revo.DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.AutoMapperModule
{
    public static class IndustryTypeModule
    {
        public static void ConfigIndustryTypeModule(this IMapperConfigurationExpression mc)
        {
            mc.CreateMap<IndustryType, IndustryTypeModel>();
            mc.CreateMap<IndustryTypeModel, IndustryType>();
                //field gen
            mc.CreateMap<IndustryType, IndustryTypeViewModel>();
            mc.CreateMap<IndustryTypeViewModel, IndustryType>();

            mc.CreateMap<IndustryType, IndustryTypeMappingIndustry>();
            mc.CreateMap<IndustryTypeMappingIndustry, IndustryType>();
        }
    }
}
