﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.Commons
{
    public class CommonConstants
    {
        public const int DefaultPaging = 10;
        public const int LimitPaging = 500;
        public const int DefaultPage = 1;
    }
}
