﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Revo.DataService.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Revo.DataService.Commons
{
    public class Worker : BackgroundService
    {
        private static readonly int IDLE_MINUTES = 5;
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger, IServiceProvider services)
        {
            _logger = logger;
            Services = services;
        }
        public IServiceProvider Services{ get; }
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while(!cancellationToken.IsCancellationRequested)
            {
                using(var scope = Services.CreateScope())
                {
                    var campaignService = scope.ServiceProvider.GetRequiredService<ICampaignService>();
                    await campaignService.ScanCampaign();   
                }
                _logger.LogInformation("Worker running at: {time}", DateTime.Now);
                await Task.Delay( 60 * 1000, cancellationToken);
            }
        }
    }
}
