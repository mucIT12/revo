﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class CampaignApplyModel
    {
        public int? ReviewerId { get; set; }
        public int? CampaignId { get; set; }
    }

    public class CampaignApplyViewModel
    {
        public static string[] Fields = {
            "Id", "ApplyDate", "Status", "ReviewerId",
            "CampaignId", "Campaign", "Reviewer"
        };
        public int? Id { get; set; }
        [BindNever]
        public DateTime? ApplyDate { get; set; }
        public int? Status { get; set; }
        public int? ReviewerId { get; set; }
        public int? CampaignId { get; set; }
        public virtual ReviewerMappingPost Reviewer { get; set; }
        public virtual CampaignDetailViewModel Campaign { get; set; }
    }
    public class CampaignApplyPostViewModel
    {
        public static string[] Fields = {
            "Id","ReviewerId", "Campaignid", "Post", "Reviewer"
        };
        public int? Id { get; set; }
        public int? Reviewerid { get; set; }
        [BindNever]
        public int? CampaignId { get; set; }
        public virtual ReviewerMappingPost Reviewer { get; set; }

        public virtual ICollection<PostMappingCampaign> Post { get; set; }
    }

    public class CampaignApplyMappingPost
    {
        public static string[] Fields = {
            "ReviewerId","Reviewer", "Campaign", "CampaignId"
        };
        [BindNever]
        public int? ReviewerId { get; set; }
        [BindNever]
        public int? CampaignId { get; set; }

        public virtual ReviewerMappingPost Reviewer { get; set; }
        public virtual CampaignMappingPost Campaign { get; set; }
    }

    public class CampaignApplyUpdateAcceptStatusModel
    {
        public int? Id { get; set; }
    }

    public class CampaignApplyUpdateCompleteStatusModel
    {
        public int? Id { get; set; }
    }
}
