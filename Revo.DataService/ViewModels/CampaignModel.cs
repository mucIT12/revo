﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using Revo.DataService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class CampaignModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string CampaignDecription { get; set; }
        public DateTime? RecruitmentDate { get; set; }
        public int? Slot { get; set; }
        public string Hashtag { get; set; }
        public string LinkBrand { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ConditionApply { get; set; }
        public string WorkDescription { get; set; }
        public int? BrandId { get; set; }
        public int? PlatformId { get; set; }
        public int? VoucherId { get; set; }
    }

    public class CampaignDetailViewModel
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string CampaignDecription { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public int? RecruitmentStatus { get; set; }
        [BindNever]
        public DateTime? RecruitmentDate { get; set; }
        [BindNever]
        public int? Applied { get; set; }
        [BindNever]
        public int? Slot { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
        [BindNever]
        public string LinkBrand { get; set; }
        [BindNever]
        public DateTime? StartDate { get; set; }
        [BindNever]
        public DateTime? EndDate { get; set; }
        [BindNever]
        public string ConditionApply { get; set; }
        [BindNever]
        public string WorkDescription { get; set; }
        [BindNever]
        public int? CampaignStatus { get; set; }
        [BindNever]
        public int? BrandId { get; set; }
        [BindNever]
        public int? PlatformId { get; set; }
        [BindNever]
        public int? VoucherId { get; set; }
        public virtual BrandMappingCampaignModel Brand { get; set; }
        public virtual PlatformMappingCampaignModel Platform { get; set; }
    }

    public class CampaignViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Image", "RecruitmentStatus", "RecruitmentDate", "CampaignStatus", "PlatformId",
            "Applied", "Slot", "StartDate", "EndDate", "Featuredstatus", "Brand", "Platform", "CampaignDecription", "Hashtag","BrandId"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string CampaignDecription { get; set; }
        public int? CampaignStatus { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
        
        [BindNever]
        public int? RecruitmentStatus { get; set; }
        public DateTime? RecruitmentDate { get; set; }
        [BindNever]
        public int? Applied { get; set; }
        [BindNever]
        public int? Slot { get; set; }
        [BindNever]
        public DateTime? StartDate { get; set; }
        [BindNever]
        public DateTime? EndDate { get; set; }
        public int? Featuredstatus { get; set; }
        public int? BrandId { get; set; }
        public int? PlatformId { get; set; }
        public virtual BrandMappingCampaignModel Brand { get; set; }
        public virtual PlatformMappingCampaignModel Platform { get; set; }
    }

    public class CampaignViewAllModel
    {
        public static string[] Fields = {
            "Id","Name", "Image", "StartDate", "EndDate", "Brand", "Platform", "Slot", "BrandId", "PlatformId",
            "CampaignStatus", "Featuredstatus", "RecruitmentDate", "RecruitmentStatus"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [BindNever]
        public string Image { get; set; }
        public int? CampaignStatus { get; set; }
        public int? Featuredstatus { get; set; }
        public int? RecruitmentStatus { get; set; }
        [BindNever]
        public DateTime? RecruitmentDate { get; set; }
        [BindNever]
        public int? Slot { get; set; }
        [BindNever]
        public DateTime? StartDate { get; set; }
        [BindNever]
        public DateTime? EndDate { get; set; }
        public int? BrandId { get; set; }
        public int? PlatformId { get; set; }
        public virtual BrandMappingCampaignModel Brand { get; set; }
        public virtual PlatformMappingCampaignModel Platform { get; set; }
    }

    public class CampaignUpdateModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int? RecruitmentStatus { get; set; }
        public DateTime? RecruitmentDate { get; set; }
        public int? Applied { get; set; }
        public int? Slot { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Featuredstatus { get; set; }
    }
    public class CampaignUpdateStatusModel
    {
        public int? Id { get; set; }
    }

    public class CampaignNameModel
    {
        public static string[] Fields = {
            "Name"
        };
        [StringAttribute]
        public string Name { get; set; }
    }
    public class CampaignMappingPost
    {
        [BindNever]
        public int? VoucherId { get; set; }
    }
}
