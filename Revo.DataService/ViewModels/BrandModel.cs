﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class BrandModel
    {
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public string Image { get; set; }
        public int? BusinessId { get; set; }
        public int? IndustryId { get; set; }
    }
    public class BrandViewModel
    {
        public static string[] Fields = {
            "Id", "Name", "Hashtag", "Industry", "Business", "Industryid", "Businessid", "Image"
        };

        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [StringAttribute]
        public string Hashtag { get; set; }
        [BindNever]
        public string Image { get; set; }
        public int? Industryid { get; set; }
        public int? Businessid { get; set; }
        public virtual IndustryMappingBrand Industry { get; set; }
        public virtual BusinessMappingBrand Business { get; set; }
    }

    public class BrandMappingCampaignModel
    {
        public static string[] Fields = {
            "Id", "Name", "Hashtag", "Industry", "Business", "Industryid", "Businessid", "Image"
        };
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public int? Industryid { get; set; }
        [BindNever]
        public int? Businessid { get; set; }
        public virtual IndustryMappingBrand Industry { get; set; }
        public virtual BusinessMappingBrand Business { get; set; }
    }

    public class BrandUpdateModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public string Image { get; set; }
        public int? BusinessId { get; set; }
        public int? IndustryId { get; set; }
    }

    public class BrandNameModel
    {
        public static string[] Fields = {
            "Name", "Id"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
    }
}
