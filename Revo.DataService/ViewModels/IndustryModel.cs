﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class IndustryModel
    {
        public string Name { get; set; }
        public string Hashtag { get; set; }
    }
    public class IndustryViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Hashtag"
        };
        public const string HiddenParams = "";
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [StringAttribute]
        public string Hashtag { get; set; }
    }

    public class IndustryMappingBrand
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
        public virtual ICollection<IndustryTypeMappingIndustry> IndustryType { get; set; }
    }
}
