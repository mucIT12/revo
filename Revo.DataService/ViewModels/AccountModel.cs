﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using Revo.DataService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Revo.DataService.ViewModels
{
    public class AccountModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class AccountViewModel
    {
        public static string[] Fields = {
            "Id","Username", "Role", "Active"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Username { get; set; }
        public bool? Active { get; set; }
        [BindNever]
        public virtual RoleMappingAccount Role { get; set; }
    }

    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class TokenModel
    {
        public int? Id { get; set; }
        public string Username { get; set; }
        public int? RoleId { get; set; }
        public string Token { get; set; }
        public string TokenType { get; set; }
        public DateTime Expires { get; set; }
    }
    public class AccountUpdateModel
    {
        public int? Id { get; set; }
        public string Password { get; set; }
        public string CurrentPassword { get; set; }
    }
    public class AccountUnbanModel
    {
        public int? Id { get; set; }
    }
    public class AccountCreateBusinessModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
