﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class BusinessModel
    {
        public string Name { get; set; }
        public string Hashtag { get; set; }
        //public string Email { get; set; }
        //public string Address { get; set; }
        //public string Image { get; set; }
        //public string Phone { get; set; }
        public int? AccountId { get; set; }
    }

    public class BusinessViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Hashtag", "Email", "Address", "Image", "Phone", "AccountId"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [StringAttribute]
        public string Hashtag { get; set; }
        [BindNever]
        public string Email { get; set; }
        [StringAttribute]
        public string Address { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string Phone { get; set; }
        [BindNever]
        public int? AccountId { get; set; }
    }

    public class BusinessMappingBrand{
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
    }
}
