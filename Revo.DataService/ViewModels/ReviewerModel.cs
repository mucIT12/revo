﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class ReviewerModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string WorkHistory { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? AccountId { get; set; }
    }

    public class ReviewerViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Address", "Image", "WorkHistory", "AccountId", "ShortName", "Email", "Phone", "Gender", "DateOfBirth"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [StringAttribute]
        public string ShortName { get; set; }
        [BindNever]
        public string Gender { get; set; }
        [BindNever]
        public DateTime? DateOfBirth { get; set; }
        [BindNever]
        public string Address { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string WorkHistory { get; set; }
        [StringAttribute]
        public string Email { get; set; }
        [StringAttribute]
        public string Phone { get; set; }
        [BindNever]

        public int? AccountId { get; set; }
    }

    public class ReviewerMappingPost
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string ShortName { get; set; }
        [BindNever]
        public string Image { get; set; }
        public virtual ICollection<PlatformMappingReviewerViewModel> PlatformMappingReviewer { get; set; }
    }

    public class ReviewerUpdateModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string WorkHistory { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class ReviewerUpdateImageModel
    {
        public int? Id { get; set; }
        public string Image { get; set; }
    }

    public class ReviewerMappingModel
    {
        [BindNever]
        public int Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string ShortName { get; set; }
        [BindNever]
        public string Gender { get; set; }
        [BindNever]
        public DateTime? DateOfBirth { get; set; }
        [BindNever]
        public string Address { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string WorkHistory { get; set; }
        [BindNever]
        public string Email { get; set; }
        [BindNever]
        public string Phone { get; set; }
    }
}
