﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class PlatformModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
    }
    public class PlatformViewModel
    {
        public static string[] Fields = {
            "Id","Name","Image"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [BindNever]
        public string Image { get; set; }
    }

    public class PlatformMappingCampaignModel
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
    }
}
