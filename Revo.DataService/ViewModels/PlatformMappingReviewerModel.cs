﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class PlatformMappingReviewerModel
    {
        public string Linkpf { get; set; }
        public int? PlatformId { get; set; }
        public int? ReviewerId { get; set; }
    }
    public class PlatformMappingReviewerViewModel
    {
        public static string[] Fields = {
            "Id","Platformid", "Reviewerid", "Platform", "Linkpf", "Reviewer"
        };
        public int? Id { get; set; }
        [BindNever]
        public string Linkpf { get; set; }
        public int? Platformid { get; set; }
        public int? Reviewerid { get; set; }
        public virtual ReviewerMappingModel Reviewer { get; set; }

        public virtual PlatformMappingCampaignModel Platform { get; set; }
    }

    public class PlatformMappingReviewerUpdateModel
    {
        public int? Id { get; set; }
        public string Linkpf { get; set; }
    }
}
