﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class RoleMappingAccount
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string RoleName { get; set; }
    }
}
