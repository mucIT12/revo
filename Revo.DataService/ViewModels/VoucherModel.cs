﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class VoucherModel
    {
        public string Image { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Discount { get; set; }
        public DateTime? EndDate { get; set; }
        public int? BrandId { get; set; }
    }
    public class VoucherViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Image", "Code", "Discount", "StartDate", "EndDate", "Status", "BrandId"
        };
        public int? Id { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string Name { get; set; }
        [StringAttribute]
        public string Code { get; set; }
        [BindNever]
        public int? Discount { get; set; }
        [BindNever]
        public DateTime? StartDate { get; set; }
        [BindNever]
        public DateTime? EndDate { get; set; }
        [BindNever]
        public int? Status { get; set; }
        [BindNever]
        public int? BrandId { get; set; }
    }

    public class VoucherMappingCampaignModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }

    public class VoucherMappingModel
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string Code { get; set; }
        [BindNever]
        public int? Discount { get; set; }
        [BindNever]
        public DateTime? StartDate { get; set; }
        [BindNever]
        public DateTime? EndDate { get; set; }
        [BindNever]
        public int? Status { get; set; }
        [BindNever]
        public int? BrandId { get; set; }
    }
}
