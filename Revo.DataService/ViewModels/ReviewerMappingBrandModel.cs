﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class ReviewerMappingBrandModel
    {
        public int? ReviewerId { get; set; }
        public int? BrandId { get; set; }
    }

    public class ReviewerMappingBrandViewModel
    {
        public static string[] Fields = {
            "Id","Reviewerid", "Brandid", "Brand"
        };
        public int? Id { get; set; }
        public int? Reviewerid { get; set; }
        public int? Brandid { get; set; }

        public virtual BrandMappingCampaignModel Brand { get; set; }
    }

    public class ReviewerMappingBrandCheckModel
    {
        public static string[] Fields = {
            "ReviewerId", "BrandId"
        };
        public int? ReviewerId { get; set; }
        [BindNever]
        public int? BrandId { get; set; }
    }
}
