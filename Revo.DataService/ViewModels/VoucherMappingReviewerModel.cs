﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class VoucherMappingReviewerModel
    {
        public int? ReviewerId { get; set; }
        public int? VoucherId { get; set; }
    }

    public class VoucherMappingReviewerViewModel
    {
        public static string[] Fields = {
            "Id","ReceiveDate", "ReviewerId", "VoucherId", "Voucher"
        };
        public int? Id { get; set; }
        [BindNever]
        public DateTime? ReceiveDate { get; set; }
        public int? ReviewerId { get; set; }
        public int? VoucherId { get; set; }
        public virtual VoucherMappingModel Voucher { get; set; }
    }
}
