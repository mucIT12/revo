﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class PostModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int? Author { get; set; }
        public int? CampaignApplyId { get; set; }

    }
    public class PostViewModel
    {
        public static string[] Fields = {
            "Id","Title", "Url", "Image", "CampaignApplyId", "CampaignApply", "Author", "ApplyDate", "ComfirmDate"
        };
        public int? Id { get; set; }
        public string Title { get; set; }
        [BindNever]
        public string Url { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public DateTime? ApplyDate { get; set; }
        [BindNever]
        public DateTime? ComfirmDate { get; set; }
        public int? Author { get; set; }
        [BindNever]
        public int? CampaignApplyId { get; set; }
        public virtual CampaignApplyMappingPost CampaignApply { get; set; }
    }

    public class PostMappingCampaign
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Title { get; set; }
        [BindNever]
        public string Image { get; set; }
        [BindNever]
        public DateTime? ApplyDate { get; set; }
        [BindNever]
        public DateTime? ComfirmDate { get; set; }
    }
    public class PostUpdateModel
    {
        public int? Id { get; set; }
    }
}
