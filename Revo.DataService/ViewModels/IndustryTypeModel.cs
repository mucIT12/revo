﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Revo.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Revo.DataService.ViewModels
{
    public class IndustryTypeModel
    {
        public string Name { get; set; }
        public string Hashtag { get; set; }
    }
    public class IndustryTypeViewModel
    {
        public static string[] Fields = {
            "Id","Name", "Hashtag"
        };
        public int? Id { get; set; }
        [StringAttribute]
        public string Name { get; set; }
        [StringAttribute]
        public string Hashtag { get; set; }
    }
    public class IndustryTypeMappingIndustry
    {
        [BindNever]
        public int? Id { get; set; }
        [BindNever]
        public string Name { get; set; }
        [BindNever]
        public string Hashtag { get; set; }
    }
}
