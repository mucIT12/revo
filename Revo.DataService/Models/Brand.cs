﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Brand
    {
        public Brand()
        {
            Campaign = new HashSet<Campaign>();
            ReviewerMappingBrand = new HashSet<ReviewerMappingBrand>();
            Voucher = new HashSet<Voucher>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public string Image { get; set; }
        public bool? Active { get; set; }
        public int? BusinessId { get; set; }
        public int? IndustryId { get; set; }

        public virtual Business Business { get; set; }
        public virtual Industry Industry { get; set; }
        public virtual ICollection<Campaign> Campaign { get; set; }
        public virtual ICollection<ReviewerMappingBrand> ReviewerMappingBrand { get; set; }
        public virtual ICollection<Voucher> Voucher { get; set; }
    }
}
