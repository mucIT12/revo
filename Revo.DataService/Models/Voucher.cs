﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Voucher
    {
        public Voucher()
        {
            Campaign = new HashSet<Campaign>();
            VoucherMappingReviewer = new HashSet<VoucherMappingReviewer>();
        }

        public int Id { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Discount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Status { get; set; }
        public bool? Active { get; set; }
        public int? BrandId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual ICollection<Campaign> Campaign { get; set; }
        public virtual ICollection<VoucherMappingReviewer> VoucherMappingReviewer { get; set; }
    }
}
