﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Account
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Active { get; set; }
        public int? RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual Business Business { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}
