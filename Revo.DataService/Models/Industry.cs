﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Industry
    {
        public Industry()
        {
            Brand = new HashSet<Brand>();
            IndustryType = new HashSet<IndustryType>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Brand> Brand { get; set; }
        public virtual ICollection<IndustryType> IndustryType { get; set; }
    }
}
