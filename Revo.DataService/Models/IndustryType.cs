﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class IndustryType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public bool? Active { get; set; }
        public int? IndustryId { get; set; }

        public virtual Industry Industry { get; set; }
    }
}
