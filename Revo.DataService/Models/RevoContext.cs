﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Revo.DataService.Models
{
    public partial class RevoContext : DbContext
    {
        public RevoContext()
        {
        }

        public RevoContext(DbContextOptions<RevoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Brand> Brand { get; set; }
        public virtual DbSet<Business> Business { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<CampaignApply> CampaignApply { get; set; }
        public virtual DbSet<Industry> Industry { get; set; }
        public virtual DbSet<IndustryType> IndustryType { get; set; }
        public virtual DbSet<Platform> Platform { get; set; }
        public virtual DbSet<PlatformMappingReviewer> PlatformMappingReviewer { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<Reviewer> Reviewer { get; set; }
        public virtual DbSet<ReviewerMappingBrand> ReviewerMappingBrand { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Voucher> Voucher { get; set; }
        public virtual DbSet<VoucherMappingReviewer> VoucherMappingReviewer { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=Revo;user id=sa;password=123;Trusted_Connection=false;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Account__RoleId__1273C1CD");
            });

            modelBuilder.Entity<Brand>(entity =>
            {
                entity.Property(e => e.Hashtag).HasMaxLength(50);

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Brand)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK__Brand__BusinessI__21B6055D");

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.Brand)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("FK__Brand__IndustryI__22AA2996");
            });

            modelBuilder.Entity<Business>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("UQ__Business__349DA5A76FC60658")
                    .IsUnique();

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Hashtag).HasMaxLength(50);

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(15);

                entity.HasOne(d => d.Account)
                    .WithOne(p => p.Business)
                    .HasForeignKey<Business>(d => d.AccountId)
                    .HasConstraintName("FK__Business__Accoun__164452B1");
            });

            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.Property(e => e.CampaignDecription).HasMaxLength(2000);

                entity.Property(e => e.ConditionApply).HasMaxLength(2000);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Hashtag)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasMaxLength(2000);

                entity.Property(e => e.LinkBrand).HasMaxLength(2000);

                entity.Property(e => e.Name).HasMaxLength(2000);

                entity.Property(e => e.RecruitmentDate).HasColumnType("date");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.WorkDescription).HasMaxLength(2000);

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.Campaign)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK__Campaign__BrandI__403A8C7D");

                entity.HasOne(d => d.Platform)
                    .WithMany(p => p.Campaign)
                    .HasForeignKey(d => d.PlatformId)
                    .HasConstraintName("FK__Campaign__Platfo__412EB0B6");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.Campaign)
                    .HasForeignKey(d => d.VoucherId)
                    .HasConstraintName("FK__Campaign__Vouche__4222D4EF");
            });

            modelBuilder.Entity<CampaignApply>(entity =>
            {
                entity.Property(e => e.ApplyDate).HasColumnType("datetime");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.CampaignApply)
                    .HasForeignKey(d => d.CampaignId)
                    .HasConstraintName("FK__CampaignA__Campa__45F365D3");

                entity.HasOne(d => d.Reviewer)
                    .WithMany(p => p.CampaignApply)
                    .HasForeignKey(d => d.ReviewerId)
                    .HasConstraintName("FK__CampaignA__Revie__44FF419A");
            });

            modelBuilder.Entity<Industry>(entity =>
            {
                entity.Property(e => e.Hashtag)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<IndustryType>(entity =>
            {
                entity.Property(e => e.Hashtag)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.IndustryType)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("FK__IndustryT__Indus__1ED998B2");
            });

            modelBuilder.Entity<Platform>(entity =>
            {
                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<PlatformMappingReviewer>(entity =>
            {
                entity.Property(e => e.Linkpf).HasMaxLength(255);

                entity.HasOne(d => d.Platform)
                    .WithMany(p => p.PlatformMappingReviewer)
                    .HasForeignKey(d => d.PlatformId)
                    .HasConstraintName("FK__PlatformM__Platf__34C8D9D1");

                entity.HasOne(d => d.Reviewer)
                    .WithMany(p => p.PlatformMappingReviewer)
                    .HasForeignKey(d => d.ReviewerId)
                    .HasConstraintName("FK__PlatformM__Revie__35BCFE0A");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.ApplyDate).HasColumnType("datetime");

                entity.Property(e => e.ComfirmDate).HasColumnType("datetime");

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.Url).HasMaxLength(255);

                entity.HasOne(d => d.CampaignApply)
                    .WithMany(p => p.Post)
                    .HasForeignKey(d => d.CampaignApplyId)
                    .HasConstraintName("FK__Post__CampaignAp__4CA06362");
            });

            modelBuilder.Entity<Reviewer>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("UQ__Reviewer__349DA5A73EDB1725")
                    .IsUnique();

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasMaxLength(15);

                entity.Property(e => e.ShortName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkHistory).HasMaxLength(500);

                entity.HasOne(d => d.Account)
                    .WithOne(p => p.Reviewer)
                    .HasForeignKey<Reviewer>(d => d.AccountId)
                    .HasConstraintName("FK__Reviewer__Accoun__1A14E395");
            });

            modelBuilder.Entity<ReviewerMappingBrand>(entity =>
            {
                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.ReviewerMappingBrand)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK__ReviewerM__Brand__267ABA7A");

                entity.HasOne(d => d.Reviewer)
                    .WithMany(p => p.ReviewerMappingBrand)
                    .HasForeignKey(d => d.ReviewerId)
                    .HasConstraintName("FK__ReviewerM__Revie__25869641");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleName).HasMaxLength(50);
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Image)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.Voucher)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK__Voucher__BrandId__29572725");
            });

            modelBuilder.Entity<VoucherMappingReviewer>(entity =>
            {
                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.HasOne(d => d.Reviewer)
                    .WithMany(p => p.VoucherMappingReviewer)
                    .HasForeignKey(d => d.ReviewerId)
                    .HasConstraintName("FK__VoucherMa__Revie__3F466844");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.VoucherMappingReviewer)
                    .HasForeignKey(d => d.VoucherId)
                    .HasConstraintName("FK__VoucherMa__Vouch__403A8C7D");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
