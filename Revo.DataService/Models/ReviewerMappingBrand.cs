﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class ReviewerMappingBrand
    {
        public int Id { get; set; }
        public int? ReviewerId { get; set; }
        public int? BrandId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}
