﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Platform
    {
        public Platform()
        {
            Campaign = new HashSet<Campaign>();
            PlatformMappingReviewer = new HashSet<PlatformMappingReviewer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Campaign> Campaign { get; set; }
        public virtual ICollection<PlatformMappingReviewer> PlatformMappingReviewer { get; set; }
    }
}
