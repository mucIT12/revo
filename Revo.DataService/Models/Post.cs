﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public DateTime? ApplyDate { get; set; }
        public DateTime? ComfirmDate { get; set; }
        public int? Status { get; set; }
        public int? Author { get; set; }
        public bool? Active { get; set; }
        public int? CampaignApplyId { get; set; }

        public virtual CampaignApply CampaignApply { get; set; }
    }
}
