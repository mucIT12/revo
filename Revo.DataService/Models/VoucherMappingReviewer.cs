﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class VoucherMappingReviewer
    {
        public int Id { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public int? ReviewerId { get; set; }
        public int? VoucherId { get; set; }

        public virtual Reviewer Reviewer { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
