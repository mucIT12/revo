﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class PlatformMappingReviewer
    {
        public int Id { get; set; }
        public string Linkpf { get; set; }
        public int? PlatformId { get; set; }
        public int? ReviewerId { get; set; }

        public virtual Platform Platform { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}
