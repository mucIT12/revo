﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class VoucherItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Discount { get; set; }
        public int? Status { get; set; }
        public bool? Active { get; set; }
        public int? VoucherId { get; set; }
        public int? CampaignApplyId { get; set; }

        public virtual CampaignApply CampaignApply { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
