﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Business
    {
        public Business()
        {
            Brand = new HashSet<Brand>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Hashtag { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? AccountId { get; set; }

        public virtual Account Account { get; set; }
        public virtual ICollection<Brand> Brand { get; set; }
    }
}
