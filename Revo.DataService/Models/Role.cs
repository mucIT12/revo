﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Role
    {
        public Role()
        {
            Account = new HashSet<Account>();
        }

        public int Id { get; set; }
        public string RoleName { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
