﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Campaign
    {
        public Campaign()
        {
            CampaignApply = new HashSet<CampaignApply>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string CampaignDecription { get; set; }
        public int? RecruitmentStatus { get; set; }
        public DateTime? RecruitmentDate { get; set; }
        public int? Applied { get; set; }
        public int? Slot { get; set; }
        public string Hashtag { get; set; }
        public string LinkBrand { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ConditionApply { get; set; }
        public string WorkDescription { get; set; }
        public int? CampaignStatus { get; set; }
        public int? FeaturedStatus { get; set; }
        public bool? Active { get; set; }
        public int? BrandId { get; set; }
        public int? PlatformId { get; set; }
        public int? VoucherId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Platform Platform { get; set; }
        public virtual Voucher Voucher { get; set; }
        public virtual ICollection<CampaignApply> CampaignApply { get; set; }
    }
}
