﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class CampaignApply
    {
        public CampaignApply()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public DateTime? ApplyDate { get; set; }
        public int? Status { get; set; }
        public int? ReviewerId { get; set; }
        public int? CampaignId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual Reviewer Reviewer { get; set; }
        public virtual ICollection<Post> Post { get; set; }
    }
}
