﻿using System;
using System.Collections.Generic;

namespace Revo.DataService.Models
{
    public partial class Reviewer
    {
        public Reviewer()
        {
            CampaignApply = new HashSet<CampaignApply>();
            PlatformMappingReviewer = new HashSet<PlatformMappingReviewer>();
            ReviewerMappingBrand = new HashSet<ReviewerMappingBrand>();
            VoucherMappingReviewer = new HashSet<VoucherMappingReviewer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string WorkHistory { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? AccountId { get; set; }

        public virtual Account Account { get; set; }
        public virtual ICollection<CampaignApply> CampaignApply { get; set; }
        public virtual ICollection<PlatformMappingReviewer> PlatformMappingReviewer { get; set; }
        public virtual ICollection<ReviewerMappingBrand> ReviewerMappingBrand { get; set; }
        public virtual ICollection<VoucherMappingReviewer> VoucherMappingReviewer { get; set; }
    }
}
